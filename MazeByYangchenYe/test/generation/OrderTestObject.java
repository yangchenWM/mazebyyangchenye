package generation;
/**
 * The stub Order object for test use only.
 * 
 * @author yangchen
 *
 */

public class OrderTestObject implements Order {
	
	private int skillLevel;
	private Builder builder;
	private boolean isPerfect;
	private Maze configuration;
	
	public OrderTestObject(int skillLevel, Builder builder, boolean isPerfect) {
		this.skillLevel = skillLevel;
		this.builder = builder;
		this.isPerfect = isPerfect;
	}

	@Override
	public int getSkillLevel() {
		return this.skillLevel;
	}

	@Override
	public Builder getBuilder() {
		return this.builder;
	}

	@Override
	public boolean isPerfect() {
		return this.isPerfect;
	}
	
	public Maze getConfig() {
		return this.configuration;
	}

	@Override
	public void deliver(Maze mazeConfig) {
		this.configuration = mazeConfig;

	}

	@Override
	public void updateProgress(int percentage) {
	}

}
