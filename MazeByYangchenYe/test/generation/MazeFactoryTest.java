package generation;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import org.junit.After;

import generation.Order.Builder;
import generation.OrderTestObject;

/**
 * Tests individual methods of the MazeBuilder class. 
 * Class contains a number of black box tests.
 * Test scenario: 
 * A 25 x 25 maze object representing the configuration of generated maze
 * 	order to construct a maze of skill level 4,
 * 	the builder is set to default DFS but might change to test other generation program
 * 	isPerfect is set to default true to exclude rooms, but might change to include rooms
 * 	generated maze is deterministic for testing purposes	
 * 
 * Test the Maze type object delivered by MazeBuilder based on functionality proided
 * by the distance class
 * 
 * @author Yangchen Ye
 *
 */
public class MazeFactoryTest { 
	
	/**
	 * an stub order object to trigger the maze generation process for testing purpose
	 */
	private Order testOrder;
	/**
	 * a maze factory object to carry out the maze generation process
	 */
	private MazeFactory factory;
	/**
	 * instead of deliver the generated maze to the gui, the maze generated in my testing
	 * environment is preserved in testMaze, so that further tests can be performed
	 */
	private Maze testMaze;
	/**
	 * the distance matrix of the generated maze
	 */
	private int[][] dist_matrix;
	/**
	 * the floorplan of the generated maze, used to test the configuration
	 * feature of the maze
	 */
	private Floorplan floorPlan;
	/**
	 * Invalid distance is the biggest integer number, which is set by the distance class to be 
	 * default distance in every cell
	 */
	private static int INVALID_DISTANCE = Integer.MAX_VALUE;
	
	@Before
	/**
	 * the setUp method is responsible for generating a deterministic maze and preserve the configuration in
	 * the field testMaze for testing use.
	 */
	public void setUp() {
		//Initialize an order
		testOrder = new OrderTestObject(4, Builder.Eller, false);
		
		//Initialize the factory to receive the order
		factory = new MazeFactory(true);
		//Order the generation of maze
		factory.order(testOrder);
		//wait till that thread terminates
		factory.waitTillDelivered();
		
		//necessary type casting to call the following method
		OrderTestObject test = (OrderTestObject) testOrder;
		//preserve the generated maze in testMaze
		testMaze = test.getConfig();
		//Preserve the distance matrix in field dist_matrix
		dist_matrix = testMaze.getMazedists().getAllDistanceValues();
		//store the floorPlan
		floorPlan = testMaze.getFloorplan();
	}
	
	@After
	/**
	 * clean up for the next test
	 */
	public void cleanUp() {
		testOrder = null;
		factory = null;
		testMaze = null;
		dist_matrix = null;
		floorPlan = null;
	}
	
	@Test
	/**
	 * this method tests that there is one and only one valid exit position which has the
	 * minimal distance, which should be one
	 * To test there is only one valid exit, traverse the dist_matrix to demonstrate there's only
	 * one cell, which identical to the exitPosition, that has distance value 1
	 * Also test that the exit position should be on the border
	 */
	public void testExitPosition() {
		//get the exitPosition genereated by the class method
		int[] exitPosition = testMaze.getMazedists().getExitPosition();
		assertFalse(exitPosition == null);
		int x = exitPosition[0];
		int y = exitPosition[1];
		//test the correctness of floorplan test method
		assertTrue(floorPlan.isExitPosition(x, y));
		//should be on the boarder
		assertTrue(x == 0 || x == testMaze.getWidth() - 1 || y == 0 || y == testMaze.getHeight() - 1);
		//distance should be 1
		assertEquals(1, testMaze.getDistanceToExit(x, y));
		
		//test that there are only one valid exit position
		for (int i = 0; i < dist_matrix.length; ++i) {
			for (int j = 0; j < dist_matrix[0].length; ++j){
				//the distance of the exist position should be 1
				if (i == x && j == y) {
					assertTrue(dist_matrix[i][j] == 1);
				}
				//the distance of any other position should be greater than 1
				//but not be invalid
				else {
					assertTrue(dist_matrix[i][j] > 1);
					assertFalse(dist_matrix[i][j] == INVALID_DISTANCE);
				}
			}
		}
	}
	
	@Test
	/**
	 * this method tests that the generated maze has one and only one valid start position,
	 * which should have the maximal distance to exist not equal to INVALID_DISTANCE
	 * All other position should not have greater distance
	 */
	public void testStartPosition() {
		//generate the startPosition by the method in distance class
		int[] startPosition = testMaze.getMazedists().getStartPosition();
		assertFalse(startPosition == null);
		int x = startPosition[0];
		int y = startPosition[1];
		assertEquals(x, testMaze.getStartingPosition()[0]);
		assertEquals(y, testMaze.getStartingPosition()[1]);
		//test that the distance value of the start position is valid and the biggest of all positions
		assertFalse(testMaze.getDistanceToExit(x, y) == INVALID_DISTANCE);
		for (int i = 0; i < testMaze.getWidth(); ++i) {
			for (int j = 0; j < testMaze.getHeight(); ++j) {
				assertTrue(testMaze.getDistanceToExit(i, j) <= testMaze.getDistanceToExit(x, y));
			}
		}
		
		//test from another angle, using the dist_matrix
		int maximalDistance = testMaze.getDistanceToExit(x, y);
		for (int i = 0; i < dist_matrix.length; ++i) {
			for (int j = 0; j < dist_matrix[0].length; ++j) {
				if (i == x && j == y) {
					assertTrue(dist_matrix[i][j] == maximalDistance);
				}
				else {
					assertTrue(dist_matrix[i][j] <= maximalDistance);
				}
			}
		}
	}
	
	@Test
	/**
	 * this method tests that there is no enclosed area in the maze, that is, it is possible
	 * to get to the exit position from any position in the maze with a finite distance
	 */
	public void testNoEnclosedArea() {
		for (int i = 0; i < testMaze.getWidth(); ++i) {
			for (int j = 0; j < testMaze.getHeight(); ++j) {
				//test that every position has a valid distance, which is a finite distance
				//from it to the exit
				assertFalse(testMaze.getDistanceToExit(i, j) == INVALID_DISTANCE);
			}
		}
	}
	
	@Test
	/**
	 * this test follows the above one to test that all position in the maze has 
	 * a path to the exit, which is done by induction as follows:
	 * for every position in the maze, suppose it's distance value is k, than, inspecting
	 * all its neighbours that have no wallboard in between, there has to be one
	 * whose distance value is k-1. If this holds for all positions except the exit,
	 * our goal is proved.
	 */
	public void testValidMazeByInduction() {

		for (int i = 0; i < testMaze.getWidth(); ++i) {
			for (int j = 0; j < testMaze.getHeight(); ++j) {
				//the distance value of the current position
				int distance = testMaze.getDistanceToExit(i, j);
				
				//if the exit position, no need to test, skip
				if (distance == 1) continue;
				
				//assert to find a neighbour position whose distance value is k-1
				boolean found = false;
				//test west neighbour
				if (i > 0) {
					if (floorPlan.hasNoWall(i, j, CardinalDirection.West)) {
						int neighbourDist = testMaze.getDistanceToExit(i-1, j);
						if (neighbourDist == distance - 1) found = true;
					}
				}
				//test east neighbour
				if (i < testMaze.getWidth() - 1) {
					if (floorPlan.hasNoWall(i, j, CardinalDirection.East)) {
						int neighbourDist = testMaze.getDistanceToExit(i+1, j);
						if (neighbourDist == distance - 1) found = true;
					}
				}
				
				//test north neighbour
				if (j > 0) {
					if (floorPlan.hasNoWall(i, j, CardinalDirection.North)) {
						int neighbourDist = testMaze.getDistanceToExit(i, j-1);
						if (neighbourDist == distance - 1) found = true;
					}
				}
				
				//test south neighbour
				if (j < testMaze.getHeight()) {
					if (floorPlan.hasNoWall(i, j, CardinalDirection.South)) {
						int neighbourDist = testMaze.getDistanceToExit(i, j+1);
						if (neighbourDist == distance - 1) found = true;
					}
				}
				
				//should find at least one qualified neighbour
				assertTrue(found);
				
			}
		}
	}
	
	@Test
	/**
	 * this method tests that the configuration of maze is at least interesting enough
	 * as to require some work
	 * I.e., the distance from the start position to the exist position is at least greater
	 * than the menhatten distance between those two point in the dist_matrix
	 */
	public void testInterestingMaze() {
		//initialize the start position
		int[] startPosition = testMaze.getMazedists().getStartPosition();
		int startx = startPosition[0];
		int starty = startPosition[1];
		
		//initialize the exit position
		int[] exitPosition = testMaze.getMazedists().getExitPosition();
		int exitx = exitPosition[0];
		int exity = exitPosition[1];
		
		//The menhatten distance between exit and start position is just the sum
		//of there difference in x and y
		int menhattenDistance = Math.abs(exitx - startx) + Math.abs(exity - starty); 
		
		assertTrue(testMaze.getDistanceToExit(startx, starty) > menhattenDistance);
	}
	
	@Test
	/**
	 * this method tests that the generated maze has intact border except at the exit position,
	 * to ensure that the generation program doesn't destruct any of its outer border so as to
	 * crash the gameplay
	 */
	public void testIntactBorder() {
		//initialize the exit position, which is an only exception
		int[] exitPosition = testMaze.getMazedists().getExitPosition();
		int exitx = exitPosition[0];
		int exity = exitPosition[1];
		
		
		//test buttom border
		for (int i = 0; i < testMaze.getWidth(); ++i) {
			//if not exit position
			if (i != exitx || testMaze.getHeight() - 1 != exity) {
				Wallboard wallboard = new Wallboard(i, testMaze.getHeight() - 1, CardinalDirection.South);
				assertTrue(floorPlan.hasWall(i, testMaze.getHeight() - 1, CardinalDirection.South));
				assertTrue(floorPlan.isPartOfBorder(wallboard));
			}
			                                                 
		}
		
		//test top border
		for (int i = 0; i < testMaze.getWidth(); ++i) {
			//if not exit position
			if (i != exitx || 0 != exity) {
				Wallboard wallboard = new Wallboard(i, 0, CardinalDirection.North);
				assertTrue(floorPlan.hasWall(i, 0, CardinalDirection.North));
				assertTrue(floorPlan.isPartOfBorder(wallboard));
			}
					                                                 
		}
		
		//test left border
		for (int j = 0; j < testMaze.getHeight(); ++j) {
			//if not exit position
			if (0 != exitx || j != exity) {
				Wallboard wallboard = new Wallboard(0, j, CardinalDirection.West);
				assertTrue(floorPlan.hasWall(0, j, CardinalDirection.West));
				assertTrue(floorPlan.isPartOfBorder(wallboard));
			}
		}
		
		//test right border
		for (int j = 0; j < testMaze.getHeight(); ++j) {
			//if not exit position
			if (testMaze.getWidth() - 1 != exitx || j != exity) {
				Wallboard wallboard = new Wallboard(testMaze.getWidth() - 1, j, CardinalDirection.East);
				assertTrue(floorPlan.hasWall(testMaze.getWidth() - 1, j, CardinalDirection.East));
				assertTrue(floorPlan.isPartOfBorder(wallboard));
			}
		}
	}
	
	@Test
	/**
	 * this method tests the robustness of room in the maze,
	 * if a cell is in a room, than it should have at most two wall board(corner) in four
	 * directions
	 */
	public void testRoom() {
		for (int x = 0; x < testMaze.getWidth(); ++x) {
			for (int y = 0; y < testMaze.getHeight(); ++y) {
				//found a cell in room, begin testing
				if (floorPlan.isInRoom(x, y)) {
					int wallCount = 0;
					for (CardinalDirection cd: CardinalDirection.values()) {
						if (floorPlan.hasWall(x, y, cd))
							wallCount++;
							
					}
					assertTrue(wallCount <= 2);
				}
			}
		}
	}
	
	@Test
	/**
	 * this method tests that the cancel method in MazeFactory class successfully
	 * and after canceling the factory can accept further orders
	 * 
	 */
	public void testCancel() {
		factory.cancel();
		assertTrue(factory.order(testOrder));
	}
	
	@Test
	/**
	 * this mthod tests that the factory reponded correctly to
	 * exceptional cases
	 * when an unimplemented builder is called, factory should reject the order
	 */
	public void testFailedBuilder() {
	
		OrderTestObject newOrder = new OrderTestObject(3, Builder.UNKNOWN, true);
		
		factory.cancel();
		
		//reject, unimplemented builder
		assertFalse(factory.order(newOrder));
		
		
	}
	@Test
	/**
	 * this method tests that the factory is able to reject new order 
	 * when the current order is being processed
	 */
	public void testTooBusyFactory() {
		factory.cancel();
		
		OrderTestObject newOrder = new OrderTestObject(3, Builder.DFS, true);
		
		factory.order(newOrder);
		
		OrderTestObject unwelcomedOrder = new OrderTestObject(3, Builder.Prim, true);
		assertFalse(factory.order(unwelcomedOrder));
	}
	
	@Test
	/**
	 * this method tests that the factory correctly responded to all valid orders
	 * with different builder
	 */
	public void testAllBuilders() {
		OrderTestObject newOrder1 = new OrderTestObject(3, Builder.DFS, true);
		OrderTestObject newOrder2 = new OrderTestObject(3, Builder.Prim, true);
		OrderTestObject newOrder3 = new OrderTestObject(3, Builder.Eller, true);
		OrderTestObject newOrder4 = new OrderTestObject(3, Builder.Kruskal, true);
		OrderTestObject newOrder5 = new OrderTestObject(3, Builder.HAK, true);
		
		//all orders should succeed
		factory.cancel();
		assertTrue(factory.order(newOrder1));
		
		factory.cancel();
		assertTrue(factory.order(newOrder2));
		
		factory.cancel();
		assertTrue(factory.order(newOrder3));
		
		factory.cancel();
		assertTrue(factory.order(newOrder4));
		
		factory.cancel();
		assertTrue(factory.order(newOrder5));

	}
	
	@Test
	/**
	 * this method tests the default constructor of the mazefactory class,
	 * which should be able to produce randomized maze by two identical orders
	 */
	public void testRandomizedFactory() {
		//initialize a factory with default constructor
		this.factory = new MazeFactory();
		//first order 
		OrderTestObject testOrder2 = new OrderTestObject(3, Builder.Kruskal, true);
		factory.order(testOrder2);
		factory.waitTillDelivered();
		Floorplan floorplan1 = testOrder2.getConfig().getFloorplan();
		
		//second order
		factory.cancel();
		factory.order(testOrder2);
		factory.waitTillDelivered();
		Floorplan floorplan2 = testOrder2.getConfig().getFloorplan();
		
		//Two generations should result in different floorplans, which is the corrrect
		//functionality of a random maze generator
		assertFalse(floorplan1.equals(floorplan2));
		
	}
	
	
	
	
	
	
	
	
	
}
