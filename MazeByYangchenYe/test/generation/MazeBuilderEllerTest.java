package generation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import generation.Order.Builder;

/**
 * this class has the responsibility to test the functionality of MazeBuilderEller class
 * It will use the generatePathways() method of that class to deliver a maze object 
 * and preserve the configuration to run test cases on.
 * The class contains a number of white box testing because it is sure that
 * the implementation tested is the Eller's algorithm
 * 
 * The general testing structure is the same as that of the MazeFactoryTest, except that
 * this class set up distinctive floorplan configurations to test the particular functionality
 * of eller's algorithm.
 * 
 * For example, Eller's algorithm is known to have difficult time dealing with rooms, so
 * this class would set up a floorplan with a huge single room in the middle, and see how
 * the algorithm does at delivering a satisfactory maze
 * 
 * Test scenario: 
 * A 25 x 25 maze object representing the configuration of generated maze
 * 	order to construct a maze of skill level 4,
 * 	the builder is Eller
 * 	isPerfect is set to default true to exclude rooms, but might change to include rooms
 * 	generated maze is deterministic for testing purposes
 * 
 * @author yangchen
 *
 */
public class MazeBuilderEllerTest{
	
	/**
	 * an stub order object to trigger the maze generation process for testing purpose
	 */
	private Order testOrder;
	
	private MazeBuilderEller builder;
	
	/**
	 * instead of deliver the generated maze to the gui, the maze generated in my testing
	 * environment is preserved in testMaze, so that further tests can be performed
	 */
	private Maze testMaze;
	
	/**
	 * a testing thread to run the builder method
	 */
	private Thread thread;
	
	/**
	 * the distance matrix of the generated maze
	 */
	private int[][] dist_matrix;
	
	/**
	 * the floorplan of the generated maze, used to test the configuration
	 * feature of the maze
	 */
	private Floorplan floorPlan;
	
	/**
	 * Invalid distance is the biggest integer number, which is set by the distance class to be 
	 * default distance in every cell
	 */
	private static int INVALID_DISTANCE = Integer.MAX_VALUE;
	
	@Before
	/**
	 * the setUp method is responsible for generating a deterministic maze and preserve the configuration in
	 * the field testMaze for testing use.
	 */
	public void setUp() {
		//Initialize an order
		testOrder = new OrderTestObject(6, Builder.Eller, false);
		
		//Initialize the builder and the thread
		builder = new MazeBuilderEller(true);
		builder.buildOrder(testOrder);
		
		thread = new Thread(builder);
	}
	
	@After
	/**
	 * clean up the setting for the next test case
	 */
	public void cleanUp() {
		testOrder = null;
		builder = null;
		thread = null;
	}
	
	/**
	 * this methods is responsible for generating the maze for testing.
	 * it is separated from the setUp method because we want to cosmutize
	 * the floorplan we want to send to Eller's algorithm to generate
	 * 
	 * therefore, the testing procedure is: setUp is automatically called, and then 
	 * each test case calls the methods to costumize the floorplan, if needed, and then
	 * calls this method themselves to complete the generation.
	 * 
	 * @throws InterruptedException
	 */
	private void generateMaze() throws InterruptedException {
		//generate the maze and wait until the background thread dies
			thread.start();
			thread.join();
			
			//necessary type casting to call the following method
			OrderTestObject test = (OrderTestObject) testOrder;
			//preserve the generated maze in testMaze
			testMaze = test.getConfig();
			//Preserve the distance matrix in field dist_matrix
			dist_matrix = testMaze.getMazedists().getAllDistanceValues();
			floorPlan = testMaze.getFloorplan();
	}	
	
	/////////////////////////Specific test cases for Eller's Algorithm////////////////////////////////////////
	/**
	 * this method sets up a single huge room of dimension width - 2 by height - 2 in the middle of the 
	 * floorplan, and following tests are based on this floorplan to see if the algorithm works well
	 * the following tests are based on this special floorplan
	 */
	private void hugeRoomSetup() {
		Floorplan fp = builder.getFloorplan();
		//initialize the huge room
		int rw = builder.getWidth() - 2;
		int rh = builder.getHeight() - 2;
		int rx = 1;
		int ry = 1;
		int rxl = rx + rw - 1;
		int ryl = ry + rh - 1;
		fp.markAreaAsRoom(rw, rh, rx, ry, rxl, ryl);
	}
	
	@Test
	/**
	 * this method tests that there is one and only one valid exit position which has the
	 * minimal distance, which should be one
	 * To test there is only one valid exit, traverse the dist_matrix to demonstrate there's only
	 * one cell, which identical to the exitPosition, that has distance value 1
	 * Also test that the exit position should be on the border
	 * @throws InterruptedException 
	 */
	public void testExitPosition() throws InterruptedException {
		this.hugeRoomSetup();
		this.generateMaze();
		//get the exitPosition genereated by the class method
		int[] exitPosition = testMaze.getMazedists().getExitPosition();
		assertFalse(exitPosition == null);
		int x = exitPosition[0];
		int y = exitPosition[1];
		//should be on the boarder
		assertTrue(x == 0 || x == testMaze.getWidth() - 1 || y == 0 || y == testMaze.getHeight() - 1);
		//distance should be 1
		assertEquals(1, testMaze.getDistanceToExit(x, y));
		
		//test that there are only one valid exit position
		for (int i = 0; i < dist_matrix.length; ++i) {
			for (int j = 0; j < dist_matrix[0].length; ++j){
				//the distance of the exist position should be 1
				if (i == x && j == y) {
					assertTrue(dist_matrix[i][j] == 1);
				}
				//the distance of any other position should be greater than 1
				//but not be invalid
				else {
					assertTrue(dist_matrix[i][j] > 1);
					assertFalse(dist_matrix[i][j] == INVALID_DISTANCE);
				}
			}
		}
	}
	
	@Test
	/**
	 * this method tests that the generated maze has one and only one valid start position,
	 * which should have the maximal distance to exist not equal to INVALID_DISTANCE
	 * All other position should not have greater distance
	 */
	public void testStartPosition() throws InterruptedException {
		this.hugeRoomSetup();
		this.generateMaze();
		//generate the startPosition by the method in distance class
		int[] startPosition = testMaze.getMazedists().getStartPosition();
		assertFalse(startPosition == null);
		int x = startPosition[0];
		int y = startPosition[1];
		//test that the distance value of the start position is valid and the biggest of all positions
		assertFalse(testMaze.getDistanceToExit(x, y) == INVALID_DISTANCE);
		for (int i = 0; i < testMaze.getWidth(); ++i) {
			for (int j = 0; j < testMaze.getHeight(); ++j) {
				assertTrue(testMaze.getDistanceToExit(i, j) <= testMaze.getDistanceToExit(x, y));
			}
		}
		
		//test from another angle, using the dist_matrix
		int maximalDistance = testMaze.getDistanceToExit(x, y);
		for (int i = 0; i < dist_matrix.length; ++i) {
			for (int j = 0; j < dist_matrix[0].length; ++j) {
				if (i == x && j == y) {
					assertTrue(dist_matrix[i][j] == maximalDistance);
				}
				else {
					assertTrue(dist_matrix[i][j] <= maximalDistance);
				}
			}
		}
	}
	
	@Test
	/**
	 * this method tests that there is no enclosed area in the maze, that is, it is possible
	 * to get to the exit position from any position in the maze with a finite distance
	 */
	public void testNoEnclosedArea() throws InterruptedException {
		this.hugeRoomSetup();
		this.generateMaze();
		for (int i = 0; i < testMaze.getWidth(); ++i) {
			for (int j = 0; j < testMaze.getHeight(); ++j) {
				//test that every position has a valid distance, which is a finite distance
				//from it to the exit
				assertFalse(testMaze.getDistanceToExit(i, j) == INVALID_DISTANCE);
			}
		}
	}
	
	@Test
	/**
	 * this method tests that the configuration of maze is at least interesting enough
	 * as to require some work
	 * I.e., the distance from the start position to the exist position is at least greater
	 * than the menhatten distance between those two point in the dist_matrix
	 */
	public void testInterestingMaze() throws InterruptedException {
		this.hugeRoomSetup();
		this.generateMaze();
		//initialize the start position
		int[] startPosition = testMaze.getMazedists().getStartPosition();
		int startx = startPosition[0];
		int starty = startPosition[1];
		
		//initialize the exit position
		int[] exitPosition = testMaze.getMazedists().getExitPosition();
		int exitx = exitPosition[0];
		int exity = exitPosition[1];
		
		//The menhatten distance between exit and start position is just the sum
		//of there difference in x and y
		int menhattenDistance = Math.abs(exitx - startx) + Math.abs(exity - starty); 
		
		assertTrue(testMaze.getDistanceToExit(startx, starty) > menhattenDistance);
	}
	
	@Test
	/**
	 * this method tests that the generated maze has intact border except at the exit position,
	 * to ensure that the generation program doesn't destruct any of its outer border so as to
	 * crash the gameplay
	 */
	public void testIntactBorder() throws InterruptedException {
		this.hugeRoomSetup();
		this.generateMaze();
		//initialize the exit position, which is an only exception
		int[] exitPosition = testMaze.getMazedists().getExitPosition();
		int exitx = exitPosition[0];
		int exity = exitPosition[1];
		
		
		//test buttom border
		for (int i = 0; i < testMaze.getWidth(); ++i) {
			//if not exit position
			if (i != exitx || testMaze.getHeight() - 1 != exity) {
				Wallboard wallboard = new Wallboard(i, testMaze.getHeight() - 1, CardinalDirection.South);
				assertTrue(floorPlan.hasWall(i, testMaze.getHeight() - 1, CardinalDirection.South));
				assertTrue(floorPlan.isPartOfBorder(wallboard));
			}
			                                                 
		}
		
		//test top border
		for (int i = 0; i < testMaze.getWidth(); ++i) {
			//if not exit position
			if (i != exitx || 0 != exity) {
				Wallboard wallboard = new Wallboard(i, 0, CardinalDirection.North);
				assertTrue(floorPlan.hasWall(i, 0, CardinalDirection.North));
				assertTrue(floorPlan.isPartOfBorder(wallboard));
			}
					                                                 
		}
		
		//test left border
		for (int j = 0; j < testMaze.getHeight(); ++j) {
			//if not exit position
			if (0 != exitx || j != exity) {
				Wallboard wallboard = new Wallboard(0, j, CardinalDirection.West);
				assertTrue(floorPlan.hasWall(0, j, CardinalDirection.West));
				assertTrue(floorPlan.isPartOfBorder(wallboard));
			}
		}
		
		//test right border
		for (int j = 0; j < testMaze.getHeight(); ++j) {
			//if not exit position
			if (testMaze.getWidth() - 1 != exitx || j != exity) {
				Wallboard wallboard = new Wallboard(testMaze.getWidth() - 1, j, CardinalDirection.East);
				assertTrue(floorPlan.hasWall(testMaze.getWidth() - 1, j, CardinalDirection.East));
				assertTrue(floorPlan.isPartOfBorder(wallboard));
			}
		}
	}
	
	@Test
	/**
	 * this method tests that Eller's algorithm can successfully generate a valid maze on the given floorplan,
	 * where success means a vaid distance matrix has been delivered
	 */
	public void testSuccessfulGeneration() throws InterruptedException {
		//generate the maze on the given floorplan
		this.hugeRoomSetup();
		this.generateMaze();
		
		//test a valid distance matrix has been delivered
		for (int i = 0; i < dist_matrix.length; ++i) {
			for (int j = 0; j < dist_matrix[0].length; ++j) {
				assertFalse(dist_matrix[i][j] == INVALID_DISTANCE);
			}
		}
	}
	
	@Test
	/**
	 * this method tests that for the special floorplan, Eller's algorithm produces a satisfactory maze
	 * such that not too many doors are created
	 * Especially, given the implementation, there should be at most one door opened on the bottom border
	 */
	public void testNotTooManyDoors() throws InterruptedException {
		//henerate the maze on the given floorplan
		this.hugeRoomSetup();
		this.generateMaze();
		
		//test that at most one door is open on the bottom border
		int doorCount = 0;
		for (int i = 1; i < testMaze.getWidth()-1; ++i) {
			if (floorPlan.hasNoWall(i, testMaze.getHeight() - 2, CardinalDirection.South)) {
				doorCount++;
			}
		}
		assertTrue(doorCount <= 1);
		
		//test that the number of doors is reasonable
		//count the number of doors
		//count top border
		for (int i = 1; i < testMaze.getWidth()-1; ++i) {
			if (floorPlan.hasNoWall(i, 1, CardinalDirection.North)) {
				doorCount++;
			}
		}
		//count left border
		for (int j = 1; j < testMaze.getHeight()-1; ++j) {
			if (floorPlan.hasNoWall(1, j, CardinalDirection.West)) {
				doorCount++;
			}
		}
		
		//count right border
		for (int j = 1; j < testMaze.getHeight()-1; ++j) {
			if (floorPlan.hasNoWall(testMaze.getWidth() - 2, j, CardinalDirection.East)) {
				doorCount++;
			}
		}
		
		//It is argued that the biggest acceptable number of doors is 2/3 of the
		//smaller one of height and width
		int threshold = Math.max(testMaze.getHeight(), testMaze.getWidth());
		
		assertTrue(doorCount <= threshold);
	}     
	
	//////////////////following tests no longer use idiosyncratic floorplan////////////
	
	@Test
	/**
	 * this methods traverses along the border and check that there is only one spot without a wall, and that spot
	 * should coinside with the exit position
	 */
	public void testOneExitArea() throws InterruptedException {
		
		this.generateMaze();
		
		int outCount = 0;
		//test top border
		for (int i = 0; i < testMaze.getWidth(); ++i) {
			//if no wall, has to be exit position
			if (floorPlan.hasNoWall(i, 0, CardinalDirection.North)) {
				assertEquals(testMaze.getMazedists().getExitPosition()[0], i);
				assertEquals(testMaze.getMazedists().getExitPosition()[1], 0);
				outCount++;
			}		                                                 
		}
		
		//test bottom border
		for (int i = 0; i < testMaze.getWidth(); ++i) {
			//if no wall, has to be exit position
			if (floorPlan.hasNoWall(i, testMaze.getHeight()-1, CardinalDirection.South)) {
				assertEquals(testMaze.getMazedists().getExitPosition()[0], i);
				assertEquals(testMaze.getMazedists().getExitPosition()[1], testMaze.getHeight()-1);
				outCount++;
			}                                                 
		}
		
		//test left border
		for (int j = 0; j < testMaze.getHeight(); ++j) {
			//if no wall, has to be exit position
			if (floorPlan.hasNoWall(0, j, CardinalDirection.West)) {
				assertEquals(testMaze.getMazedists().getExitPosition()[0], 0);
				assertEquals(testMaze.getMazedists().getExitPosition()[1], j);
				outCount++;
			}		                                                 
		}
		
		//test left border
		for (int j = 0; j < testMaze.getHeight(); ++j) {
			//if no wall, has to be exit position
			if (floorPlan.hasNoWall(testMaze.getWidth()-1, j, CardinalDirection.East)) {
				assertEquals(testMaze.getMazedists().getExitPosition()[0], testMaze.getWidth()-1);
				assertEquals(testMaze.getMazedists().getExitPosition()[1], j);
				outCount++;
			}		                                                 
		}
		
		assertEquals(1, outCount);
	}
	
	@Test
	/**
	 * this method tests the randomized constroctor of MazeBuilderEller class,
	 * which should generate two different floorplans after two generatePathways method
	 * this test is simplified as to just call the generatePathways method and
	 * see if the floorplan stored as the field of MazeBuilderEller class has been
	 * modified correctly
	 */
	public void testRandomizedGeneration() {
		
		builder = new MazeBuilderEller();
		//first generate
		builder.buildOrder(testOrder);
		builder.generatePathways();
		Floorplan fp1 = builder.getFloorplan();
		
		//second generate
		builder = new MazeBuilderEller();
		//first generate
		builder.buildOrder(testOrder);
		builder.generatePathways();
		Floorplan fp2 = builder.getFloorplan();
		
		assertFalse(fp1.equals(fp2));
		
	}
	
	@Test
	/**
	 * this methods tests that there are no loops outside of room as follows:
	 * for any for-cell unit, (x, y)(x+1, y)(x, y+1)(x+1, y+1), if it is not the
	 * case that all of them are in a room, than it should not be the case that 
	 * there are not any single wall seperating them
	 */
	public void testNoLoopOutsideRoom() throws InterruptedException {
		this.generateMaze();
		for (int x = 0; x < testMaze.getWidth() - 1; ++x) {
			for (int y = 0; y < testMaze.getHeight() - 1; ++y) {
				//if they are all in one room, skip
				if (floorPlan.isInRoom(x, y) && floorPlan.isInRoom(x, y+1)
						&& floorPlan.isInRoom(x+1, y) && floorPlan.isInRoom(x+1, y+1)) 
					continue;
				//at least one wallboard should be found in any such unit
				assertFalse(floorPlan.hasNoWall(x, y, CardinalDirection.East)
						&& floorPlan.hasNoWall(x, y, CardinalDirection.South)
						&& floorPlan.hasNoWall(x+1, y, CardinalDirection.South)
						&& floorPlan.hasNoWall(x, y+1, CardinalDirection.East));
			}
		}
	}
	
}

