package gui;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import generation.Order.Builder;
import gui.Robot.Direction;

/**
 * This class is responsible for running test cases for the wizard class that instantiates the robot driver interface.
 * 
 * setUp:
 * Create a controller for test purpose, a test robot and a robot driver instantiated with an object of the Wizard
 * class. Bond them together, and run the controller, let the driver algorithm operate in this environment.
 * 
 * 1. Test Basic Consistency:
 * 		The Wizard driving algorithm is not supposed to thorw any exception or error no matter what (assuming there is always at
 * 		least one working sensors). It should be able to deal with anything happening during the game and deliver a result either 
 * 		true or false
 * 2. Test Correctness of Algorithm:
 * 		The Wizard is an efficient maze solving algorithm, so it is expected to successfully escape the maze with 3000 initial battery
 * 		level. Further, the pathlength it goes through is expected to be less than the distance from start position to exit, because jump
 * 		is utilized to reduce pathlength.
 * 
 * @author yangchen
 *
 */
public class WizardTest {
	
	Controller testController;
	
	Robot testRobot;
	
	Wizard wizard;
	
	@Before
	public void setUp(){
		
		testController = new Controller();
		testRobot = new BasicRobot();
		wizard = new Wizard();
		
		testController.setRobotAndDriver(testRobot, wizard);
		testController.setBuilder(Builder.Eller);
		testController.setSkillLevel(4);
		
	}

	@After
	public void tearDown(){
		
		testController = null;
		testRobot = null;
		wizard = null;
		
	}
	
	////////////////////////////////Test static functionality//////////////////////////////////////////
	
	@Test
	/**
	 * The pathlength counter of the driver should return exactly the same value as 
	 * the robot's odometer reading
	 */
	public void testGetPathlength() {
		//initial value equal
		wizard.setRobot(testRobot);
		assertEquals(wizard.getPathLength(), testRobot.getOdometerReading());
		
		testRobot.setBatteryLevel(100);
		testController.switchFromTitleToGenerating();
		testController.waitTillDriverFinished();
		
		//final value equal after running
		assertEquals(wizard.getPathLength(), testRobot.getOdometerReading());
		
	}
	
	@Test
	/**
	 * The energyconsumption returned by the robot driver should confirm with the robot's
	 * battery level, such that energyConsumption + batterylevel = initialBatteryLevel
	 */
	public void testGetEnergyConsumption() {
		testRobot.setBatteryLevel(500);
		wizard.setRobot(testRobot);
		assertEquals(500, (int)testRobot.getBatteryLevel() + (int)wizard.getEnergyConsumption());
		
		testController.switchFromTitleToGenerating();
		testController.waitTillDriverFinished();
		
		assertEquals(500, (int)testRobot.getBatteryLevel() + (int)wizard.getEnergyConsumption());
		
	}
	
	////////////////////////////////Test Basic Consistency/////////////////////////////////////////////
	@Test
	/**
	 * this method tests that the wall follower algorithm can run without exception given a robot and
	 * a maze to work on. In this case none of the robot's sensors is broken, so there is impossible to be
	 * any unsupported operation exception. The robot driver should be able to handle everything during the
	 * game and return either true or false without any unresolved  problems.
	 */
	public void testNormalCaseNoException() {
		try {
			testRobot.setBatteryLevel(500);
			testController.switchFromTitleToGenerating();
			testController.waitTillDriverFinished();
		} catch (Throwable e) {
			fail("Robot driver fail to successfully deal with problems");
		}
	}
	
	@Test
	/**
	 * thie method tests that the robot driver can successfully handle the situation where the robot's left
	 * sensor is not operational. It should not throw any exception along the way
	 */
	public void testOnlyForwardNoException() {
		testRobot.triggerSensorFailure(Direction.LEFT);
		testRobot.triggerSensorFailure(Direction.BACKWARD);
		testRobot.triggerSensorFailure(Direction.RIGHT);
		testRobot.setBatteryLevel(1000);
		try {
			testController.switchFromTitleToGenerating();
			testController.waitTillDriverFinished();
		} catch (Throwable e) {
			fail("Robot driver fail to successfully deal with problems");
		}
	}
	
	@Test
	/**
	 * thie method tests that the robot driver can successfully handle the situation where the robot's forward
	 * sensor is not operational. It should not throw any exception along the way
	 */
	public void testOnlyLeftNoException() {
		testRobot.triggerSensorFailure(Direction.FORWARD);
		testRobot.triggerSensorFailure(Direction.BACKWARD);
		testRobot.triggerSensorFailure(Direction.RIGHT);
		testRobot.setBatteryLevel(1000);
		try {
			testController.switchFromTitleToGenerating();
			testController.waitTillDriverFinished();
		} catch (Throwable e) { //any exception or error, fail
			fail("Robot driver fail to successfully deal with problems");
		}
	}
	
	@Test
	/**
	 * this method tests that the robot driver can successfully handle the situation where the robot's forward
	 * sensor and left sensor is not operational. It should not throw any exception along the way
	 */
	public void testOnlyRightNoException() {
		testRobot.triggerSensorFailure(Direction.FORWARD);
		testRobot.triggerSensorFailure(Direction.LEFT);
		testRobot.triggerSensorFailure(Direction.BACKWARD);
		testRobot.setBatteryLevel(1000);
		try {
			testController.switchFromTitleToGenerating();
			testController.waitTillDriverFinished();
		} catch (Throwable e) {//any exception or error, fail
			fail("Robot driver fail to successfully deal with problems");
		}
	}
	
	@Test
	/**
	 * this method tests that the robot driver can successfully handle the situation where the only operational
	 * sensor is the backward one.
	 */
	public void testOnlyBackSensorNoException() {
		testRobot.triggerSensorFailure(Direction.FORWARD);
		testRobot.triggerSensorFailure(Direction.LEFT);
		testRobot.triggerSensorFailure(Direction.RIGHT);
		testRobot.setBatteryLevel(1000);
		try {
			testController.switchFromTitleToGenerating();
			testController.waitTillDriverFinished();
		} catch (Throwable e) {//any exception or error, fail
			fail("Robot driver fail to successfully deal with problems");
		}
	}
	
	/////////////////////////////////////////Test Correctness///////////////////////////////////////////////////
	@Test
	/**
	 * This method tests that the wizard is correctly implemented in two respects:
	 * 1. It should be able to escape a level 4 maze with 3000 energy
	 * 2. The pathlength it uses should be less than the distance from start to exit
	 */
	public void testCorrectnessOfWizard() {
		
		testController.switchFromTitleToGenerating();
		testController.waitTillDriverFinished();
		//assert the game is success
		assertTrue(((StateEnding)testController.currentState).gameWinned());
		
		int pathlength = wizard.getPathLength();
		int[] start = testController.getMazeConfiguration().getStartingPosition();
		int distance = testController.getMazeConfiguration().getDistanceToExit(start[0], start[1]);
		
		assertTrue(pathlength < distance);
		
	}
}
