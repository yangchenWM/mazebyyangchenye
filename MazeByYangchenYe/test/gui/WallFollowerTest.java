package gui;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import generation.Order.Builder;
import gui.Robot.Direction;

/**
 * 
 * This class has the responsibility to run test cases for the WallFollower Class in a testing environment
 * 
 * setUp:
 * Create a controller for test purpose, a test robot and a robot driver instantiated with an object of the WallFollower
 * class. Bond them together, and run the controller, let the driver algorithm operate in this environment.
 * 
 * 1. test consistency:
 *		An WallFollower object is expected to work perfectly when a RobotDriver type object is expected by the controller,
 *		which is the liskov's substitution principle as WallFollower is a type that implements the RobotDriver interface
 * 2. test consistency of driving algorithm:
 * 		The test of basic consistency of the driving algorithm used in WallFollower class consists in testing that once the algorithm
 * 		has started working, no exception should be thrown, everything should be handled correctly within the scope of the method. For
 * 		example, if the method throws an unsupportedOperationException, that is wrong because it should handle everything well inside.
 * 		This should hold when outer thread manually fails and repairs sensor at the runtime. Whatever happens, no exception should be
 * 		thrown.
 * 3. test correctness of the return value of driving algorithm:
 * 		If the driving method returns false, indicating that the game is lost, then the robot's batterylevel should reflect and confirm
 * 		this. Otherwise if the driving method returns true, the stateEnding and the robot should confirm this.
 * 4. test the basic functionality of the driving algorithm:
 * 		For the wall follower algorithm, it is not guaranteed to find the path out of the maze due to energy limit. However, in some cases
 * 		it should guarantee to find a solution.
 * 		4.1 set the robot directly at the exit position
 * 		4.2 give the robot infinite amount of energy and a perfect maze
 * 		Test that in the above two cases, the driving algorithm should succeed in finding the solution
 * @author yangchen
 *
 */
public class WallFollowerTest {
	
	Controller testController;
	
	Robot testRobot;
	
	WallFollower wallFollower;
	
	@Before
	public void setUp(){
		
		testController = new Controller();
		testRobot = new BasicRobot();
		wallFollower = new WallFollower();
		
		testController.setRobotAndDriver(testRobot, wallFollower);
		testController.setBuilder(Builder.Eller);
		testController.setSkillLevel(4);
		
	}

	@After
	public void tearDown(){
		
		testController = null;
		testRobot = null;
		wallFollower = null;
		
	}

	///////////////////////Test Static Functionalities///////////////////////////
	@Test
	/**
	 * The pathlength counter of the driver should return exactly the same value as 
	 * the robot's odometer reading
	 */
	public void testGetPathlength() {
		//initial value equal
		wallFollower.setRobot(testRobot);
		assertEquals(wallFollower.getPathLength(), testRobot.getOdometerReading());
		
		testRobot.setBatteryLevel(100);
		testController.switchFromTitleToGenerating();
		testController.waitTillDriverFinished();
		
		//final value equal after running
		assertEquals(wallFollower.getPathLength(), testRobot.getOdometerReading());
		
	}
	
	@Test
	/**
	 * The energyconsumption returned by the robot driver should confirm with the robot's
	 * battery level, such that energyConsumption + batterylevel = initialBatteryLevel
	 */
	public void testGetEnergyConsumption() {
		testRobot.setBatteryLevel(500);
		wallFollower.setRobot(testRobot);
		assertEquals(500, (int)testRobot.getBatteryLevel() + (int)wallFollower.getEnergyConsumption());
		
		testController.switchFromTitleToGenerating();
		testController.waitTillDriverFinished();
		
		assertEquals(500, (int)testRobot.getBatteryLevel() + (int)wallFollower.getEnergyConsumption());
		
	}
	
	////////////////////Test Basic Consistency////////////////////////////////////
	
	@Test
	/**
	 * this method tests that the wall follower algorithm can run without exception given a robot and
	 * a maze to work on. In this case none of the robot's sensors is broken, so there is impossible to be
	 * any unsupported operation exception. The robot driver should be able to handle everything during the
	 * game and return either true or false without any unresolved  problems.
	 */
	public void testNormalCaseNoException() {
		try {
			testRobot.setBatteryLevel(500);
			testController.switchFromTitleToGenerating();
			testController.waitTillDriverFinished();
		} catch (Throwable e) {
			fail("Robot driver fail to successfully deal with problems");
		}
	}
	
	@Test
	/**
	 * thie method tests that the robot driver can successfully handle the situation where the robot's left
	 * sensor is not operational. It should not throw any exception along the way
	 */
	public void testNoLeftSensorNoException() {
		testRobot.triggerSensorFailure(Direction.LEFT);
		testRobot.setBatteryLevel(1000);
		try {
			testController.switchFromTitleToGenerating();
			testController.waitTillDriverFinished();
		} catch (Throwable e) {
			fail("Robot driver fail to successfully deal with problems");
		}
	}
	
	@Test
	/**
	 * thie method tests that the robot driver can successfully handle the situation where the robot's forward
	 * sensor is not operational. It should not throw any exception along the way
	 */
	public void testNoForwardSensorNoException() {
		testRobot.triggerSensorFailure(Direction.FORWARD);
		testRobot.setBatteryLevel(1000);
		try {
			testController.switchFromTitleToGenerating();
			testController.waitTillDriverFinished();
		} catch (Throwable e) {
			fail("Robot driver fail to successfully deal with problems");
		}
	}
	
	@Test
	/**
	 * this method tests that the robot driver can successfully handle the situation where the robot's forward
	 * sensor and left sensor is not operational. It should not throw any exception along the way
	 */
	public void testNoLeftForwardSensorNoException() {
		testRobot.triggerSensorFailure(Direction.FORWARD);
		testRobot.triggerSensorFailure(Direction.LEFT);
		testRobot.setBatteryLevel(1000);
		try {
			testController.switchFromTitleToGenerating();
			testController.waitTillDriverFinished();
		} catch (Throwable e) {
			fail("Robot driver fail to successfully deal with problems");
		}
	}
	
	@Test
	/**
	 * this method tests that the robot driver can successfully handle the situation where the only operational
	 * sensor is the backward one.
	 */
	public void testOnlyBackSensorNoException() {
		testRobot.triggerSensorFailure(Direction.FORWARD);
		testRobot.triggerSensorFailure(Direction.LEFT);
		testRobot.triggerSensorFailure(Direction.RIGHT);
		testRobot.setBatteryLevel(1000);
		try {
			testController.switchFromTitleToGenerating();
			testController.waitTillDriverFinished();
		} catch (Throwable e) {
			fail("Robot driver fail to successfully deal with problems");
		}
	}
	
	///////////////////////Test Correctness//////////////////////////////
	
	@Test
	/**
	 * This method tests that the algorithm should successfully take the robot out of the maze when
	 * the robot has infinite energy and the maze is relatively small(just for time convenience)
	 */
	public void testCorrectnessOfAlgorithmToGetOutOfMaze() {
		
		testController.setSkillLevel(3);
		testRobot.setBatteryLevel(Float.POSITIVE_INFINITY);
		
		testController.switchFromTitleToGenerating();
		testController.waitTillDriverFinished();
		
		//assert the game is won
		assertTrue(((StateEnding)testController.currentState).gameWinned());
		
	}
	
	
	

}
