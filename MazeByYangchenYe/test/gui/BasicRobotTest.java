package gui;


import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import generation.CardinalDirection;
import gui.Robot.Direction;
import gui.Robot.Turn;

import java.util.Random;

/**
 * This class has the responsibility to test the functionality of the BasicRobot class and its intractions with
 * controller class. The test structure is designed in the following way
 * 
 * 1. Test setup: maintain two objects, a controller and a basicrobot. The controller is assumed to have adjusted 
 * so that the robot object can perform operations in its playing state. Start the controller and navigate to the
 * stagePlaying. Note: if any robot method is called when the controller's current state is not statePlaying, that is
 * an error. Then test the robot method on that background.
 * 
 * 2. test sensor methods: 
 * A basic criteria for the correctness of sensor methods of the robot is consistency. The information returned by its
 * sensor should match the information stored at the maze state exactly. Therefore, my primary testing structure is to
 * see if those two are consistent with each other.
 * 
 * 3. test actuator methods:
 * Actuator methods in the basicRobot class should successfully update the state of the robot reflected both in the sensor methods
 * and the in maze state.
 * 
 * 4. test internal state:
 *
 * 
 * @author yangchen
 *
 */
public class BasicRobotTest {
	
	/**
	 * the controller for test purpose
	 */
	private Controller testController;
	
	/**
	 * the robot object to be tested
	 */
	private BasicRobot testRobot;
	
	/**
	 * assist in generating random move
	 */
	private Random rand;

	@Before
	/**
	 * set up testing fixture
	 */
	public void setUp(){
		
		rand = new Random();
		
		//initialize test objects
		testController = new Controller();
		
		testRobot = new BasicRobot();
		
		testController.setRobotAndDriver(testRobot, null);
		//turn off the graphic functionality 
		//dry-run the controller for testing purpose
		//testController.turnOffGraphics();
		
		//start controller and
		//navigate to the playing stage
		//by simulating a user input telling the controller
		//to generate a level 5 maze
		//wait until generation is completed
		testController.start();
		testController.setSkillLevel(5);
		testController.switchFromTitleToGenerating();
		
		
		//now generation is completed and the controller is in
		//state of playing, set the reference to the maze in testRobot
		//testRobot.setMazeConfig();
		
		
		
	}

	@After
	/**
	 * tear down fixture for next test
	 */
	public void tearDown(){
		rand = null;
		testController = null;
		testRobot = null;
	}
	
	/**
	 * this method randomly select an operation for the robot to perform, including
	 * 1. move a random distance
	 * 2. rotate to a direction
	 * 3. jump
	 */
	private void robotRandomOperation() {
		//don't want the program to need to worry about battery level
		//reset every time to ensure that the robot will not stop
		testRobot.setBatteryLevel(3000);
		int random = rand.nextInt(3);
		switch(random) {
		//jump
		case 1:
			testRobot.jump();
			break;
		//move
		case 2:
			random = rand.nextInt(5);
			testRobot.move(random);
			break;
		//rotate
		default:
			int dice = rand.nextInt(3);
			if (dice == 0) testRobot.rotate(Turn.LEFT);
			else if (dice == 1) testRobot.rotate(Turn.RIGHT);
			else testRobot.rotate(Turn.AROUND);
			
		}
	}
	
	///////////////////////////////////////////Test Sensor///////////////////////////////////////////////////////////
	
	@Test
	/**
	 * this method tests that the robot knows where it is by having a correct
	 * getCurrentPosition method. The returned value is compared to that stored in the
	 * controller and two are supposed to be exactly the same
	 */
	public void testCurrentPosition() {
		
		//do the test 10 times, and do some random operations in between
		for (int i = 0; i < 10; ++i) {
			//the current position returned by the robot
			int[] curRobot = testRobot.getCurrentPosition();
			//current position recognized by the maze
			int[] curMaze = testController.getCurrentPosition();
		
			assertEquals(curMaze[0], curRobot[0]);
			assertEquals(curMaze[1], curRobot[1]);
			
			this.robotRandomOperation();
		}
	}
	
	@Test
	/**
	 * this method tests that the robot knows where it is facing by having a correct
	 * getCurrentPosition method. The returned value is compared to that stored in the
	 * controller and two are supposed to be exactly the same
	 */
	public void testCurrentDirection() {
		
		//do the test 10 times, do some random operations in between
		for (int i = 0; i < 10; ++i) {
			CardinalDirection cd = testRobot.getCurrentDirection();
		
			assertEquals(testController.getCurrentDirection(), cd);
			
			this.robotRandomOperation();
		}
	}
	
	@Test
	/**
	 * this method repeatedly call the accompanying method after doing some random operations
	 * to repeatedly test the correctness of the canSeeThroughTheExitIntoEternity() method.
	 * this test involves deliberately setting the robot at the exit position and
	 * see if its sensor method works correctly, and then let the robot do random operations
	 * and see if its sensor method works correctly
	 * Do the test repeatedly in different maze settings
	 */
	public void testCanSeeThroughTheExitIntoEternity(){
		
		//do the test 10 times to overcome the implication
		//of random maze generation
		for (int i = 0; i < 10; ++i) {
			int[] exit = testController.getMazeConfiguration().getMazedists().getExitPosition();
			
			testController.setCurrentPosition(exit[0], exit[1]);
		
			assertTrue(testRobot.canSeeThroughTheExitIntoEternity(Direction.FORWARD)
					|| testRobot.canSeeThroughTheExitIntoEternity(Direction.BACKWARD)
					|| testRobot.canSeeThroughTheExitIntoEternity(Direction.LEFT)
					|| testRobot.canSeeThroughTheExitIntoEternity(Direction.RIGHT));
		
			for (Direction d: Direction.values()) {
				if (testRobot.canSeeThroughTheExitIntoEternity(d)) {
					//hand control to the test of the specific direction
					this.testCanSeeThroughTheExitIntoEternityDirection(d);
					break;
				}
			}
			
		}
		
		
	}
	

	/**
	 * this method tests the functionality of the robot's exit sensor in a given direction d,
	 * in the following way:
	 * if the robot senses an exit in a direction d, then turn to that direction and continue
	 * moving; at some point it is expected to trigger an assertion error because the robot would
	 * have gone out of the maze and the maze is no longer playable
	 * 
	 * @param d
	 */
	private void testCanSeeThroughTheExitIntoEternityDirection(Direction d) {
		
		switch(d) {
		case FORWARD:
			break;
		case LEFT:
			testRobot.rotate(Turn.LEFT);
			break;
		case RIGHT:
			testRobot.rotate(Turn.RIGHT);
			break;
		case BACKWARD:
			testRobot.rotate(Turn.AROUND);
			break;
		}
		
		//continue moving toward the direction and expect to see and assertion error
		
		try{
			while (true) {
			
			//if somehow the robot stopped, the test fails
			assertFalse(testRobot.hasStopped());
			
			testRobot.move(1);
				
			}
		} catch(AssertionError e) {
			assertTrue(this.testController.currentState == this.testController.states[3]);
			//success
			//set up a new game and return
			this.setUp();
		}
		
		
	}
	
	
	@Test
	/**
	 * this method tests that the robot can correctly identify whether it is at the exit position
	 */
	public void testIsAtExit() {
		
		int[] exit = this.testController.getMazeConfiguration().getMazedists().getExitPosition();
		
		//do the test 20 times, do random operations in between
		for (int i = 0; i < 20; ++i) {
			
			int[] currentPosition = testRobot.getCurrentPosition();
			
			//if current position actually is the exit position, the robot class method should return true
			if (currentPosition[0] == exit[0]
					&& currentPosition[1] == exit[1]) assertTrue(testRobot.isAtExit());
			//otherwise the robot class method should rturn false
			else assertFalse(testRobot.isAtExit());
			
			this.robotRandomOperation();
		}
		
		//deliberately set the robot at the exit position
		//and see if the method acts correctly
		testController.setCurrentPosition(exit[0], exit[1]);
		assertTrue(testRobot.isAtExit());
		
	}
	
	@Test
	/**
	 * this method tests that the robot can correctly identify whether it is in the room
	 */
	public void testIsInRoom() {
		//do the test 20 times, do random operations in between
		for (int i = 0; i < 20; ++i) {
			
			int[] currentPosition = testRobot.getCurrentPosition();
				
			//if the position is actually in the room, expect the robot class method
			//to return true
			if (this.testController.getMazeConfiguration().getFloorplan().isInRoom(currentPosition[0], currentPosition[1]))
				assertTrue(testRobot.isInsideRoom());
			//otherwise expect the robot class method to return false
			else assertFalse(testRobot.isInsideRoom());
			
			this.robotRandomOperation();
		}
	}
	
	@Test
	/**
	 * this method tests the basic consistency of the robot distanceToObstacle method,
	 * in that a distance forward should be the same as the distance right if the robot turns left
	 */
	public void testBasicDistanceToObstacle() {
		
		//test consistency for forward direction distance
		int x = testRobot.distanceToObstacle(Direction.FORWARD);
		testRobot.rotate(Turn.AROUND);
		assertEquals(x, testRobot.distanceToObstacle(Direction.BACKWARD));
		
		x = testRobot.distanceToObstacle(Direction.FORWARD);
		testRobot.rotate(Turn.LEFT);
		assertEquals(x, testRobot.distanceToObstacle(Direction.RIGHT));
		
		x = testRobot.distanceToObstacle(Direction.FORWARD);
		testRobot.rotate(Turn.RIGHT);
		assertEquals(x, testRobot.distanceToObstacle(Direction.LEFT));
		
		
		//test consistency for right direction distance
		int y = testRobot.distanceToObstacle(Direction.RIGHT);
		testRobot.rotate(Turn.RIGHT);
		assertEquals(y, testRobot.distanceToObstacle(Direction.FORWARD));
		
		y = testRobot.distanceToObstacle(Direction.RIGHT);
		testRobot.rotate(Turn.LEFT);
		assertEquals(y, testRobot.distanceToObstacle(Direction.BACKWARD));
		
		y = testRobot.distanceToObstacle(Direction.RIGHT);
		testRobot.rotate(Turn.AROUND);
		assertEquals(y, testRobot.distanceToObstacle(Direction.LEFT));
	}
	
	@Test
	/**
	 * this method repeatedly calls the private testing method that tests the functionality
	 * of distanceToObstacle method once, and do some random operation in between
	 * this test is to test the numerical accuracy of the robot's distanceToObstacle method.
	 */
	public void testDistanceToObstacle() {
		//do the test 10 times, randomly operate the robot in between
		for (int i = 0; i < 10; ++i) {
			this.testDistanceToObstacleOnce();
			this.robotRandomOperation();
		}
	}
	
	/**
	 * this method tests the correctness of distanceToObstacle method by the following ways:
	 * for each of the four directions, ask for a value by that method.
	 * If the returned value is not Integer.Max_Value, go by the direction, 
	 * and inductively, the value returned by distanceToObstacle should be decremented
	 * by 1 up till it gets to 0
	 */
	private void testDistanceToObstacleOnce() {
		
		this.testDistanceForward();
		this.testDistanceLeft();
		this.testDistanceRight();
		this.testDistanceBack();
		
	}
	
	/**
	 * this method breaks down the testDistanceOnce method to first test the Forward direction.
	 */
	private void testDistanceForward() {
		
		int forwardDistance = testRobot.distanceToObstacle(Direction.FORWARD);
		if (forwardDistance != Integer.MAX_VALUE) {
			//move toward the expected wall
			while (forwardDistance != 0) {
				testRobot.move(1);
				
				int newForwardDistance = testRobot.distanceToObstacle(Direction.FORWARD);
				//distance should decrement by 1;
				assertEquals(newForwardDistance + 1, forwardDistance);
				//update
				forwardDistance = newForwardDistance;
			}
			//move further, and expect to see no move happens because there is a wall in front
			testRobot.move(1);
			forwardDistance = testRobot.distanceToObstacle(Direction.FORWARD);
			assertEquals(0, forwardDistance);
			
			//confirm there is a wall in front
			int[] curPos = testRobot.getCurrentPosition();
			CardinalDirection cd = testRobot.getCurrentDirection();
			assertTrue(testController.getMazeConfiguration().getFloorplan().hasWall(curPos[0], curPos[1], cd));
			
		}
	}
	
	/**
	 * this method then tests the left direction
	 */
	private void testDistanceLeft() {
		
		int leftDistance = testRobot.distanceToObstacle(Direction.LEFT);
		if (leftDistance != Integer.MAX_VALUE) {
			//should be able to reach the wall by first rotating left and go straight along
			testRobot.rotate(Turn.LEFT);
			assertEquals(leftDistance, testRobot.distanceToObstacle(Direction.FORWARD));
			//move toward the expected wall
			while (leftDistance != 0) {
				testRobot.move(1);
				
				int newLeftDistance = testRobot.distanceToObstacle(Direction.FORWARD);
				//distance should decrement by 1;
				assertEquals(newLeftDistance + 1, leftDistance);
				//update
				leftDistance = newLeftDistance;
			}
			//move further, and expect to see no move happens because there is a wall in front
			testRobot.move(1);
			leftDistance = testRobot.distanceToObstacle(Direction.FORWARD);
			assertEquals(0, leftDistance);
			
			//confirm there is a wall in front
			int[] curPos = testRobot.getCurrentPosition();
			CardinalDirection cd = testRobot.getCurrentDirection();
			assertTrue(testController.getMazeConfiguration().getFloorplan().hasWall(curPos[0], curPos[1], cd));
		}
	}
	
	/**
	 * this method then tests the right direction
	 */
	private void testDistanceRight() {
		
		int rightDistance = testRobot.distanceToObstacle(Direction.RIGHT);
		if (rightDistance != Integer.MAX_VALUE) {
			//should be able to reach the wall by first rotating right and go straight along
			testRobot.rotate(Turn.RIGHT);
			assertEquals(rightDistance, testRobot.distanceToObstacle(Direction.FORWARD));
			//move toward the expected wall
			while (rightDistance != 0) {
				testRobot.move(1);
				
				int newRightDistance = testRobot.distanceToObstacle(Direction.FORWARD);
				//distance should decrement by 1;
				assertEquals(newRightDistance + 1, rightDistance);
				//update
				rightDistance = newRightDistance;
			}
			//move further, and expect to see no move happens because there is a wall in front
			testRobot.move(1);
			rightDistance = testRobot.distanceToObstacle(Direction.FORWARD);
			assertEquals(0, rightDistance);
			
			//confirm there is a wall in front
			int[] curPos = testRobot.getCurrentPosition();
			CardinalDirection cd = testRobot.getCurrentDirection();
			assertTrue(testController.getMazeConfiguration().getFloorplan().hasWall(curPos[0], curPos[1], cd));
		}
	}
	
	/**
	 * this method then tests the backward direction
	 */
	private void testDistanceBack() {
		
		int backDistance = testRobot.distanceToObstacle(Direction.BACKWARD);
		if (backDistance != Integer.MAX_VALUE) {
			//should be able to reach the wall by first rotating right and go straight along
			testRobot.rotate(Turn.AROUND);
			assertEquals(backDistance, testRobot.distanceToObstacle(Direction.FORWARD));
			//move toward the expected wall
			while (backDistance != 0) {
				testRobot.move(1);
				
				int newBackDistance = testRobot.distanceToObstacle(Direction.FORWARD);
				//distance should decrement by 1;
				assertEquals(newBackDistance + 1, backDistance);
				//update
				backDistance = newBackDistance;
			}
			//move further, and expect to see no move happens because there is a wall in front
			testRobot.move(1);
			backDistance = testRobot.distanceToObstacle(Direction.FORWARD);
			assertEquals(0, backDistance);
			
			//confirm there is a wall in front
			int[] curPos = testRobot.getCurrentPosition();
			CardinalDirection cd = testRobot.getCurrentDirection();
			assertTrue(testController.getMazeConfiguration().getFloorplan().hasWall(curPos[0], curPos[1], cd));
		}
	}
	
	//////////////////////////////////////////Test Actuator//////////////////////////////////////////////////////
	@Test
	/**
	 * this method tests the basic correctness of rotate() method, and whether it is consistent
	 * with the maze representation.
	 * Basically, a rotate right method should change the direction, so the current direction
	 * after a rotate is expected to be different from the one before it.
	 * Then, the relation between the direction befor rotate left and after is expected to be:
	 * cd_before = cd_after.rotateClockwise, which should be perfectly matched by the robot operation
	 */
	public void testRotateRight() {
		CardinalDirection cd = testRobot.getCurrentDirection();
		testRobot.rotate(Turn.RIGHT);
		
		assertTrue(cd != testRobot.getCurrentDirection());
		assertTrue(cd == testRobot.getCurrentDirection().rotateClockwise());
	}
	
	@Test
	/**
	 * this method tests the basic correctness of rotate() method, and whether it is consistent
	 * with the maze representation.
	 * Basically, a rotate around method should change the direction, so the current direction
	 * after a rotate is expected to be different from the one before it.
	 * Then, the relation between the direction befor rotate left and after is expected to be:
	 * cd_before = cd_after.oppositeDirection, which should be perfectly matched by the robot operation
	 */
	public void testRotateAround() {
		CardinalDirection cd = testRobot.getCurrentDirection();
		testRobot.rotate(Turn.AROUND);
		
		assertTrue(cd != testRobot.getCurrentDirection());
		assertTrue(cd == testRobot.getCurrentDirection().oppositeDirection());
	}
	
	@Test
	/**
	 * this method tests the basic correctness of rotate() method, and whether it is consistent
	 * with the maze representation.
	 * Basically, a rotate left method should change the direction, so the current direction
	 * after a rotate is expected to be different from the one before it.
	 * Then, the relation between the direction befor rotate left and after is expected to be:
	 * cd_before = cd_after.oppositeDirection.rotateClockwise, which should be perfectly matched by the robot operation
	 */
	public void testRotateLeft() {
		CardinalDirection cd = testRobot.getCurrentDirection();
		testRobot.rotate(Turn.LEFT);
		
		assertTrue(cd != testRobot.getCurrentDirection());
		assertTrue(cd == testRobot.getCurrentDirection().oppositeDirection().rotateClockwise());
	}
	
	
	
	@Test
	/**
	 * this method repeated calls the private test method, and make random operations in between
	 */
	public void testBunchOfRotate() {
		//test 10 times
		for (int i = 0; i < 10; ++i) {
			this.testRotateIntegrative();
			this.robotRandomOperation();
		}
	}
	

	/**
	 * this method tests the correctness of rotate method in a integrative way, consisting
	 * of several rotates and several moves, to see each rotate generates correct outcome
	 * and they can be performed consecutively without interfering each other.
	 * 1. South --rotate left-- West
	 *    South --rotate right-- East
	 * 2. East --rotate left-- South
	 * 	  East --rotate right-- North
	 * 3. West --rotate left-- North
	 * 	  West --rotate right-- South
	 * 4. North --rotate left-- East
	 *    North --rotate right-- west
	 */
	private void testRotateIntegrative() {
		
		CardinalDirection cd = testRobot.getCurrentDirection();
		//rotate left
		testRobot.rotate(Turn.LEFT);
		CardinalDirection rotated = testRobot.getCurrentDirection();
		//check correct outcome
		switch(cd) {
		case East:
			assertTrue(rotated == CardinalDirection.South);
			break;
		case West:
			assertTrue(rotated == CardinalDirection.North);
			break;
		case South:
			assertTrue(rotated == CardinalDirection.West);
			break;
		case North:
			assertTrue(rotated == CardinalDirection.East);
			break;
		}
		
		cd = rotated;
		
		//rotate around
		testRobot.rotate(Turn.AROUND);
		rotated = testRobot.getCurrentDirection();
		//check correct outcome
		switch(cd) {
		case East:
			assertTrue(rotated == CardinalDirection.West);
			break;
		case West:
			assertTrue(rotated == CardinalDirection.East);
			break;
		case South:
			assertTrue(rotated == CardinalDirection.North);
			break;
		case North:
			assertTrue(rotated == CardinalDirection.South);
			break;
		}
		
		cd = rotated;
		
		//rotate right
		testRobot.rotate(Turn.RIGHT);
		rotated = testRobot.getCurrentDirection();
		//check correct outcome
		switch(cd) {
		case East:
			assertTrue(rotated == CardinalDirection.North);
			break;
		case West:
			assertTrue(rotated == CardinalDirection.South);
			break;
		case South:
			assertTrue(rotated == CardinalDirection.East);
			break;
		case North:
			assertTrue(rotated == CardinalDirection.West);
			break;
		}
	}
	
	@Test
	/**
	 * this method tests that the robot can move correctly, the criteria for which is as follows:
	 * 1. if it does not has a wall right in front, a move(1) call should upate its current position
	 * without changing its current direction. 
	 * 2. if it does has a wall right in front, a walk should not do anything except consuming battery
	 */
	public void testBasicMove() {
		//do the test 10 times
		for (int i = 0; i < 10; ++i) {
			
			//preserve the current position and direction
			CardinalDirection cd = testRobot.getCurrentDirection();
			
			int[] cur = testRobot.getCurrentPosition();
			
			int curx = cur[0];
			int cury = cur[1];
			
			int distanceForward = testRobot.distanceToObstacle(Direction.FORWARD);
			//make the move
			testRobot.move(1);
			
			//store the position after move
			int[] dest = testRobot.getCurrentPosition();
			
			int destx = dest[0];
			int desty = dest[1];
			
			//if the distance is not 0, move is expected to have correct updating effect
			if (distanceForward != 0) {
				
				//the position should be progressed in the correct way
				assertTrue(destx == curx || desty == cury);
				assertTrue(Math.abs(destx - curx) == 1 || Math.abs(desty - cury) == 1);
				
				//direction should not change
				assertEquals(cd, testRobot.getCurrentDirection());
			}
			
			//if distance is 0, move should not update either position or direction
			else {
				assertTrue(destx == curx && desty == cury);
				assertEquals(cd, testRobot.getCurrentDirection());
			}
			
			//randomly update the maze in between
			this.robotRandomOperation();
			
		}
	}
	
	@Test
	/**
	 * this method tests that the move operation is correct under some more intricated situations
	 * 1. A move with distance greater than 1 can be done correctly, in that the robot should
	 * advance a right number of cells (hit into wall or not).
	 * 2. Moves can interact well with rotate, i.e., the robot can perform arbitrary permuations of
	 * rotates and moves, and it will figure out the right thing to do.
	 * 3. Moves should not be performed if the robot has been stopped.
	 */
	public void testAdvancedMove() {
		//do the test 10 times to reduce randomness
		for (int i = 0; i < 10; ++i) {
			
			int[] beforeMove = testRobot.getCurrentPosition();
			
			//if no battery, move should not have any effect
			testRobot.setBatteryLevel(0);
			assertTrue(testRobot.hasStopped());
			
			try{
				testRobot.move(1);
			} catch(AssertionError e) {
				//success reset the game
				this.setUp();
			}
			
			
			//restore initial position before move
			beforeMove = testRobot.getCurrentPosition();
			
			//a move with distance greater than or equal to 1 should result in position update
			//min(distance, distanceToObstacle(Forward))
			int dist = testRobot.distanceToObstacle(Direction.FORWARD);
			if (dist != Integer.MAX_VALUE && dist != 0) {
				testRobot.move(dist);
				
				//the position should be updated correctly in that only one index should be updated
				assertFalse(beforeMove[0] == testRobot.getCurrentPosition()[0]
						&& beforeMove[1] == testRobot.getCurrentPosition()[1]);
				assertTrue(beforeMove[0] == testRobot.getCurrentPosition()[0]
						|| beforeMove[1] == testRobot.getCurrentPosition()[1]);
				
				//the changed position should be dist - 1 since no wallboard is hit
				assertTrue(Math.abs(testRobot.getCurrentPosition()[0] - beforeMove[0]) == dist
						|| Math.abs(testRobot.getCurrentPosition()[1] - beforeMove[1]) == dist);
			}
			
			//restore initial position before move
			beforeMove = testRobot.getCurrentPosition();
			
			//test the interaction between move and rotate
			//and that the robot should not go on moving when the 
			//wall is hit
			dist = testRobot.distanceToObstacle(Direction.LEFT);
			if (dist != Integer.MAX_VALUE) {
				
				testRobot.rotate(Turn.LEFT);
				
				testRobot.move(dist + 20);
				
				//the changed position should be dist since a wallboard is hit
				//and the robot has stopped 
				assertTrue(beforeMove[0] == testRobot.getCurrentPosition()[0]
						|| beforeMove[1] == testRobot.getCurrentPosition()[1]);	
				assertTrue(Math.abs(testRobot.getCurrentPosition()[0] - beforeMove[0]) == dist
						|| Math.abs(testRobot.getCurrentPosition()[1] - beforeMove[1]) == dist);
				
			}
	
			//randomly update robot's position
			this.robotRandomOperation();
		}
	}
	
	@Test(expected = AssertionError.class)
	/**
	 * this method tests that a move cannot succeed given a negative distance
	 */
	public void testMoveFailure() {
		testRobot.move(-1);
	}
	
	@Test
	/**
	 * this method tests the basic correctness of jump operation of the robot
	 * in cases where the jump should succeed
	 * In such cases, the jump is expected to update the robot's position by 1 no matter what,
	 * and the direction should remain unchanged
	 * 
	 */
	public void testBasicJump() {
		
		//do the test 10 times
		for (int i = 0; i < 10; ++i) {
			
			int width = testController.getMazeConfiguration().getWidth();
			int height = testController.getMazeConfiguration().getHeight();
			
			//start the test by placing the robot at a central position where
			//it is impossible to go out of bound
			testController.setCurrentPosition(width/2, height/2);
			
			int[] beforeJump = testRobot.getCurrentPosition();
			CardinalDirection cd = testRobot.getCurrentDirection();
			
			testRobot.jump();
			
			//test the outcome of jump
			assertFalse(beforeJump[0] == testRobot.getCurrentPosition()[0]
					&& beforeJump[1] == testRobot.getCurrentPosition()[1]);
			assertTrue(beforeJump[0] == testRobot.getCurrentPosition()[0]
					|| beforeJump[1] == testRobot.getCurrentPosition()[1]);
			
			//should advanced one cell
			assertTrue(Math.abs(testRobot.getCurrentPosition()[0] - beforeJump[0]) == 1
					|| Math.abs(testRobot.getCurrentPosition()[1] - beforeJump[1]) == 1);
			
			//direction should remain the same
			assertEquals(cd, testRobot.getCurrentDirection());
			
			this.robotRandomOperation();
		}
		
	}
	
	@Test
	/**
	 * this method tests the robustness of robot's jump operation:
	 * 1. when facing the outer border of the maze, the jump method should have no
	 * effect
	 * 1.1. when the robot is called to jump more than width/height times, its final
	 * position should be right in front of the border
	 * 2. the robot should not jump if there's no enough battery
	 */
	public void testFailedJump() {
		
		//do the test 10 times
		for (int i = 0; i < 10; ++i) {
			
			int width = testController.getMazeConfiguration().getWidth();
			int height = testController.getMazeConfiguration().getHeight();
			
			//set an upper bound of jumps which is definitely impossible
			//to finish without going out of bound
			int invalidJump = width + height;
			
			//in this test no need to worry about battery level
			testRobot.setBatteryLevel(100000);
			
			for (int j = 0; j < invalidJump; ++j) {
				testRobot.jump();
			}
			
			//test the outcome of jump
			int[] afterJump = testRobot.getCurrentPosition();
			//position should remain valid
			assertTrue(afterJump[0] < width && afterJump[1] < height);
			
			//final position should land on the border
			assertTrue(afterJump[0] == width - 1
					|| afterJump[1] == height - 1
					|| afterJump[0] == 0
					|| afterJump[1] == 0);
			
			this.robotRandomOperation();
		}
	}
	
	//////////////////////////Test Internal State Method////////////////////////
	
	@Test
	/**
	 * test the robot can correctly set and return it's current battery level
	 */
	public void testGetAndSetBatteryLevel() {
		
		//test correctness of getter method after setting
		testRobot.setBatteryLevel(1000);
		assertTrue(1000 == testRobot.getBatteryLevel());
		
		//test correctness of getter method after moving
		//assumption: energy for one step forward is 5
		//energy for sensing is 1
		int dist = testRobot.distanceToObstacle(Direction.FORWARD);
		testRobot.move(dist);
		assertTrue((1000 - testRobot.getEnergyForStepForward() * dist - 1) == testRobot.getBatteryLevel());
		
		//test correctness of getter method after rotating
		//assumption: energy for a full rotation is 12
		testRobot.setBatteryLevel(3000);
		testRobot.rotate(Turn.LEFT);
		assertTrue(3000 - testRobot.getEnergyForFullRotation()/4 == testRobot.getBatteryLevel());
		
		testRobot.setBatteryLevel(3000);
		testRobot.rotate(Turn.RIGHT);
		assertTrue(3000 - testRobot.getEnergyForFullRotation()/4 == testRobot.getBatteryLevel());
		
		testRobot.setBatteryLevel(3000);
		testRobot.rotate(Turn.AROUND);
		assertTrue(3000 - testRobot.getEnergyForFullRotation()/2 == testRobot.getBatteryLevel());
		
		//test correctness of getter method after jumping
		//assumption: energy for a jump is 50
		testRobot.setBatteryLevel(3000);
		//relocate the robot to ensure that a jump would succeed
		int width = testController.getMazeConfiguration().getWidth();
		int height = testController.getMazeConfiguration().getHeight();
		testController.setCurrentPosition(width/2, height/2);
		
		testRobot.jump();
		assertTrue(2950 == testRobot.getBatteryLevel());
	
	}
	
	@Test
	/**
	 * this method tests that the robot can correctly return and reset its
	 * odometer that keeps track of how long it has gone through
	 * the basic functionality is tested here and special cases
	 * are handled in the next test case
	 */
	public void testGetAndResetOdoMeter() {
		
		int width = testController.getMazeConfiguration().getWidth();
		int height = testController.getMazeConfiguration().getHeight();
		
		//a reset and read pair should work fine
		testRobot.resetOdometer();
		assertEquals(0, testRobot.getOdometerReading());
		
		int dist = testRobot.distanceToObstacle(Direction.FORWARD);
		if (dist != Integer.MAX_VALUE) {
			
			testRobot.move(dist);
			
			assertEquals(dist, testRobot.getOdometerReading());
		
		}
		
		//a rotation should not update odo meter
		int beforeRotate = testRobot.getOdometerReading();
		testRobot.rotate(Turn.AROUND);
		assertEquals(beforeRotate, testRobot.getOdometerReading());
		
		//a jump, if successful, should update odo meter by no more than
		//one
		testController.setCurrentPosition(width/2, height/2);
		int beforeJump = testRobot.getOdometerReading();
		testRobot.jump();
		assertEquals(beforeJump+1, testRobot.getOdometerReading());
		
		//reset
		testRobot.resetOdometer();
		assertEquals(0, testRobot.getOdometerReading());
		
	}
	
	@Test
	/**
	 * this method tests that the robot would not update its odometer
	 * when the move or jump failed
	 */
	public void testFailedOdometerUpdate() {
		
		//for this test cases no need to worry about energy
		testRobot.setBatteryLevel(100000);
		
		int width = testController.getMazeConfiguration().getWidth();
		int height = testController.getMazeConfiguration().getHeight();
		
		int invalidMove = width + height;
		
		//push the robot to a wall so that it will be impossible to move
		//successfully and update the odometer
		testRobot.move(invalidMove);
		
		int beforeMove = testRobot.getOdometerReading();
		
		testRobot.move(1);
		//should be equal to before
		assertEquals(beforeMove, testRobot.getOdometerReading());
		
		//push the robot to the border so that it will be impossible
		//to jump successfully and update the odometer
		for (int i = 0; i < invalidMove; ++i) testRobot.jump();
		
		int beforeJump = testRobot.getOdometerReading();
		testRobot.jump();
		//should not update odometer
		assertEquals(beforeJump, testRobot.getOdometerReading());
		
		
	}
	
	@Test
	/**
	 * this method tests the getEnergyForFullRotation, getEnergyForStepForward
	 * and hasRoomSensor method, which are all trivial in my specific implementation
	 */
	public void testTrivialGetters() {
		
		assertEquals(12, (int)testRobot.getEnergyForFullRotation());
		
		assertEquals(5, (int)testRobot.getEnergyForStepForward());
		
		assertTrue(testRobot.hasRoomSensor());
	}
	
	@Test
	/**
	 * this method tests the correctness of the robot's hasStopped method
	 * and its interaction with other methods.
	 * Once the robot has stopped, any call of any robot method that depends
	 * on a robot playing in a maze should fail and throw an assertion error
	 * however, battery level and odo meter reading can still be accessed because
	 * they are internal states of the robot and do not depends upon it playing in
	 * a maze or not
	 */
	public void testHasStopped() {
		
		testRobot.setBatteryLevel(-1);
		assertTrue(testRobot.hasStopped());
		
		//call to sensor method is expected to fail
		try {
			testRobot.distanceToObstacle(Direction.FORWARD);
			fail("exception failed to be generated");
		} catch (AssertionError e){
			//nothing to do, continue testing
		}
		
		try {
			testRobot.distanceToObstacle(Direction.LEFT);
			fail("exception failed to be generated");
		} catch (AssertionError e){
			//nothing to do, continue testing
		}
		
		try {
			testRobot.distanceToObstacle(Direction.RIGHT);
			fail("exception failed to be generated");
		} catch (AssertionError e){
			//nothing to do, continue testing
		}
		
		try {
			testRobot.distanceToObstacle(Direction.BACKWARD);
			fail("exception failed to be generated");
		} catch (AssertionError e){
			//nothing to do, continue testing
		}
		
		//call to see if exit is visible should also fail
		try {
			testRobot.canSeeThroughTheExitIntoEternity(Direction.FORWARD);
			fail("exception failed to be generated");
		} catch (AssertionError e){
			//nothing to do, continue testing
		}
		
		try {
			testRobot.canSeeThroughTheExitIntoEternity(Direction.LEFT);
			fail("exception failed to be generated");
		} catch (AssertionError e){
			//nothing to do, continue testing
		}
		
		try {
			testRobot.canSeeThroughTheExitIntoEternity(Direction.RIGHT);
			fail("exception failed to be generated");
		} catch (AssertionError e){
			//nothing to do, continue testing
		}
		
		try {
			testRobot.canSeeThroughTheExitIntoEternity(Direction.BACKWARD);
			fail("exception failed to be generated");
		} catch (AssertionError e){
			//nothing to do, continue testing
		}
		
		//all actuators should fail
		
		try {
			testRobot.move(1);;
			fail("exception failed to be generated");
		} catch (AssertionError e){
			//nothing to do, continue testing
		}
		
		try {
			testRobot.jump();
			fail("exception failed to be generated");
		} catch (AssertionError e){
			//nothing to do, continue testing
		}
		
		try {
			testRobot.rotate(Turn.LEFT);
			fail("exception failed to be generated");
		} catch (AssertionError e){
			//nothing to do, continue testing
		}
		
		try {
			testRobot.getCurrentPosition();
			fail("exception failed to be generated");
		} catch(AssertionError e) {
			//nothing to do, continue testing
		}
		
		try {
			testRobot.getCurrentDirection();
			fail("exception failed to be generated");
		} catch(AssertionError e) {
			//nothing to do, continue testing
		}
		
		try {
			testRobot.isAtExit();
			fail("exception failed to be generated");
		} catch(AssertionError e) {
			//nothing to do, continue testing
		}
		
		try {
			testRobot.isInsideRoom();
			fail("exception failed to be generated");
		} catch(AssertionError e) {
			//nothing to do, continue testing
		}
		
		//the following method call should succeed
		try {
			testRobot.getBatteryLevel();
		} catch(Exception e) {
			fail("should not have any exception");
		}
		
		try {
			testRobot.getOdometerReading();
		} catch(Exception e) {
			fail("should not have any exception");
		}
		
		try {
			testRobot.resetOdometer();
		} catch(Exception e) {
			fail("should not have any exception");
		}
		
		try {
			testRobot.hasOperationalSensor(Direction.BACKWARD);
		} catch(Exception e) {
			fail("should not have any exception");
		}
		
		try {
			testRobot.triggerSensorFailure(Direction.FORWARD);
		} catch(Exception e) {
			fail("should not have any exception");
		}
		
		try {
			testRobot.repairFailedSensor(Direction.FORWARD);
		} catch(Exception e) {
			fail("should not have any exception");
		}
		
		
		
		
	}
	

	@Test
	/**
	 * this method calls the private helper method to test the sensor property of
	 * all four directions in the same manner
	 */
	public void testSensorProperties() {
		this.testSensorOperationalDirection(Direction.FORWARD);
		this.testSensorOperationalDirection(Direction.LEFT);
		this.testSensorOperationalDirection(Direction.RIGHT);
		this.testSensorOperationalDirection(Direction.BACKWARD);
	}
	
	
	/**
	 * this method tests the functionality of the robot to control whether
	 * its directional sensors are operational or not. It is operational in default,
	 * but when it is triggered failure, then any call of sensor methods in left
	 * direction should fail and throw an exception. If it is repaired some time later
	 * everything should get ready and work again
	 */
	private void testSensorOperationalDirection(Direction d) {
		
		//true in default
		assertTrue(testRobot.hasOperationalSensor(d));
		
		//trigger failure
		testRobot.triggerSensorFailure(d);
		assertFalse(testRobot.hasOperationalSensor(d));
		
		try {
			testRobot.distanceToObstacle(d);
			fail("fail to detect exception");
		} catch(UnsupportedOperationException e) {
			//nothing to do, continue testing
		}
		
		
		//repair should return true because
		//in this implementation there are always operational sensors
		assertTrue(testRobot.repairFailedSensor(d));
		
		//this time method call should succeed
		try {
			testRobot.distanceToObstacle(d);
		} catch(UnsupportedOperationException e) {
			fail("fail to repair sensor");
		}

	}
	
	/////////////////Test of Exceptional cases and some small remainder cases///////////////////////
	
	@Test
	/**
	 * test the functionality of customized constructor
	 */
	public void testCustomizedConstructor() {
		this.tearDown();
		
		testRobot = new BasicRobot(100000);
		
		assertEquals(100000, (int)testRobot.getBatteryLevel());
		
	}
	
	@Test(expected = AssertionError.class)
	/**
	 * this method tests that the robot can correctly raise an error if anyone
	 * tries to bind it to a null controller
	 */
	public void testNullControllerError() {
		
		testRobot.setMaze(null);
		
	}
	
	@Test(expected = AssertionError.class)
	/**
	 * this method tests that a robot would not do any job when its controller
	 * is not in the playable state
	 */
	public void testInvalidControllerState() {
		
		Controller invalidController = new Controller();
		
		testRobot.setMaze(invalidController);
		
		testRobot.getCurrentPosition();
		
	}
	
	@Test(expected = AssertionError.class)
	/**
	 * this method tests that the robot would not do any job when it is not
	 * bonded to any controller
	 */
	public void testNullControllerRobot() {
		
		testRobot = new BasicRobot();
		
		testRobot.move(1);
		
	}
	
	@Test
	/**
	 * this method tests that the maze game can successfully end because of absence of energy:
	 * by continuously calling the move, rotate, jump, and sensor method, the robot is expected
	 * to finally exhaust its battery and the maze game should end
	 */
	public void testMazeSuccessfulyEnded() {
		
		//continually move
		try {
			
			//for this test we don't want the robot to have too high battery level,
			//which would slow down the test
			testRobot.setBatteryLevel(100);
			
			while (true) testRobot.move(1);
			
			
		} catch (AssertionError e) {
			//test succeeded, start a new one
			testController.switchToTitle();
			testController.setSkillLevel(6);
			testController.switchFromTitleToGenerating();
		}
		
		//continually rotate
		try {
			
			testRobot.setBatteryLevel(10);
			
			while (true) testRobot.rotate(Turn.LEFT);
		} catch (AssertionError e) {
			//test succeeded, start a new one
			testController.switchToTitle();
			testController.setSkillLevel(6);
			testController.switchFromTitleToGenerating();
		}
		
		
		//continually jump
		try {
					
			testRobot.setBatteryLevel(100);
					
			while (true) testRobot.jump();
		} catch (AssertionError e) {
			//test succeeded, start a new one
			testController.switchToTitle();
			testController.setSkillLevel(6);
			testController.switchFromTitleToGenerating();
		}
		
		
		//continually sensing
		try {
					
			testRobot.setBatteryLevel(100);
					
			while (true) testRobot.distanceToObstacle(Direction.FORWARD);
		} catch (AssertionError e) {
			//nothing to do, test succeeded
		}
		
		
		
	}
	

	
	
	
}
