package generation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * This class has the responsibility to create a maze of given dimensions (width, height) 
* together with a solution based on a distance matrix.
* The MazeBuilderEller implements Runnable such that it can be run a separate thread.
* The MazeFactory has a MazeBuilder and handles the thread management.   

* 
* The maze is built with Eller's algorithm. 
* Which is to construct the maze row by row.
* First initialize the first row of the maze with each cell belonging to separate sets,
* then randomly connect adjacent cells to be in one set and delete the wallboard between them.
* After that randomly choose at least one cell of each set to delete its South board to make vertical connection to the next row,
* and add the newly connected cell into the set.
* Repeat this process until the last row.
* In the last row connect and merge every adjacent cell that does not belong to the same set.
* 
*   
* @author yangchen
*/
public class MazeBuilderEller extends MazeBuilder implements Runnable {
	
	/**
	 * hashmap that maps the setNumber to a list of cell object, one set number can map to a list of
	 * different cells
	 */
	private HashMap<Integer, List<Cell>> setToCell;
	
	/**
	 * hash map that maps a cell to a unique set number. Multiple cells can map to the same number
	 */
	private HashMap<Cell, Integer> cellToSet;
	
	/**
	 * assist in choosing unique set number, incremented everytime a new set is added
	 */
	private static Integer nextSetNumber = 0;
	
	/**
	 * default constructer to generate a randomized maze by eller's algorithm
	 */
	public MazeBuilderEller() {
		super();
		setToCell = new HashMap<Integer, List<Cell>>();
		cellToSet = new HashMap<Cell, Integer>();
		System.out.println("\"MazeBuilderEller uses Eller's algorithm to generate maze.\"");
	}
	/**
	 * generate a randomized or deterministic maze according to the argument
	 * @param det indicates whether deterministic or not
	 */
	public MazeBuilderEller(boolean det) {
		super(det);
		setToCell = new HashMap<Integer, List<Cell>>();
		cellToSet = new HashMap<Cell, Integer>();
		System.out.println("\"MazeBuilderEller uses Eller's algorithm to generate maze.\"");
	}
	
	/*@Override
	protected int generateRooms(){
		return 0;
	}*/
	
	
	@Override
	/**
	 * this method generates pathways using eller's algorithm
	 * For each row except the last one, do three things:
	 * first, populate each cell in the row with a set, if a cell does not have set,
	 * then add it to a new set, if it does, skip
	 * second, randomly join adjacent cells by making horizontal connection and merge
	 * their sets to be one,
	 * third, randomly build vertical connections to the next cell, only ensuring that
	 * at least one cell in any set is vertically extended to the next row.
	 * For the last row, merge every adjacent cells to be one set and horizontally connects them.
	 */
	protected void generatePathways() {
		
		//For every row except the last, do three things as specified in the doc:
		//populate every cell, make horizontal connection randomly, and make
		//vertical connection randomly
		for (int y = 0; y < height - 1; ++y) {
			this.populate(y);
			this.horizontalConnect(y);
			this.verticalConnect(y);
		}
		
		//For the last row, just populate and horizontally connect every cell from
		//different set. No need to vertically connect anything.
		this.populate(height-1);
		this.connectAll(height-1);
	}
	/**
	 * this method populate each cell in a row,
	 * if a cell has no set, then add it to its own new set,
	 * if a cell has a set already, do nothing
	 * @param y the row index of the operation
	 */
	private void populate(int y) {
		//for each cell in a row
		for (int x = 0; x < width; ++x) {
			Cell cell = new Cell(x, y);
			//add only if it does not have a cell yet
			if (cellToSet.get(cell) == null) {
				this.addNewSet(cell);
			}
		}
	}
	
	/**
	 * this method decides what to do with a populated row,
	 * it randomly connect adjacent cells and merge them to one set, but only
	 * if they do not belong to the same set
	 * The probability of random horizontal merge is set to 50%
	 * @param y the row index of the operation
	 */
	private void horizontalConnect(int y) {
		Wallboard wallboard = new Wallboard(0, 0, CardinalDirection.East);
		for (int x = 0; x < width - 1; ++x) {

			Cell cell = new Cell(x, y);
			//there are 50% probability that horizontal connection will happen
			int flip  = random.nextIntWithinInterval(0, 1);
			Cell adjacent = new Cell(x+1, y);
			
			//Case1: the current cell is in a room and the adjacent cell is also in a room,
			//then merge because they have already been connected, don't delete the wall board
			//since there are no wall board, just continue to the next cell.
			if ((int)cellToSet.get(cell) != (int)cellToSet.get(adjacent) && floorplan.isInRoom(x, y) && floorplan.isInRoom(x+1, y)) {
				this.merge(cell, adjacent);
				continue;
			}
		
			//Case2: the current cell is not in a room and the adjacent cell is not in a room,
			//in which case the wallboard between them can be deleted, just randomly choose whether
			//or not to do this.
			//Case3: One cell is in a room and another cell is not in a room, and the wallboard between them
			//happens to be a door, in which case the wallboard can be deleted, just randomly choose whether or not
			//to do this.
			//Case4: one cell is in a room and another cell is not, and the allboard between them is not
			//a door, then don't merge
			wallboard.setLocationDirection(x, y, CardinalDirection.East);
			if ((int)cellToSet.get(cell) != (int)cellToSet.get(adjacent) && floorplan.canTearDown(wallboard) && flip > 0) {
				this.merge(cell, adjacent);
				floorplan.deleteWallboard(wallboard);
			}
			
		}
	}

	/**
	 * this method randomly connect the row y to its next row by randomly choosing
	 * at least one cell in each set to connect to the cell below it,
	 * and merge that cell to the same set
	 * @param y the row index to operate on
	 */
	private void verticalConnect(int y) {
		
		//initialize the hashmap for the next row, after this method is completed,
		//two fields of the class should be updated to reference to these two,
		//because we're only concerned about one row, and the row that has been 
		//merged and operated on is of no use
		HashMap<Integer, List<Cell>> newSetToCell = new HashMap<Integer, List<Cell>>();
		HashMap<Cell, Integer> newCellToSet = new HashMap<Cell, Integer>();
		
		//initialize a wallboard for use
		Wallboard wallboard = new Wallboard(0, 0, CardinalDirection.South);
		
		//for every setNumber of this row
		for (Integer setNumber: setToCell.keySet()) {
			
			List<Cell> set = setToCell.get(setNumber);
			//the same set number should be of use in the next row,
			//so build the mapping in advance
			List<Cell> setOfNextRow = new ArrayList<Cell>();
			newSetToCell.put(setNumber, setOfNextRow);
			
			//For every set, first randomly choose one cell to vertically
			//connect to the next row, regardless whether the wallboard is a
			//border, Eller's algorithm takes priority here
			int indexToconnect = random.nextIntWithinInterval(0, set.size()-1);
			Cell cellToConnect = set.get(indexToconnect);
			wallboard.setLocationDirection(cellToConnect.getX(), cellToConnect.getY(), CardinalDirection.South);
			floorplan.deleteWallboard(wallboard);
			Cell connectedCell = new Cell(cellToConnect.getX(), cellToConnect.getY() + 1);
			newCellToSet.put(connectedCell, setNumber);
			setOfNextRow.add(connectedCell);
			set.remove(indexToconnect);
			
			//then for every other cell in the set, if it is above a room border or in a room
			//don't connect, otherwise, randomly choose whether to connect or not
			//in both cases, remove the visited cell from the set
			//the vertical merge probability is set to be 20% to avoid too long vertical pathways
			while (set.size() != 0) {
				Cell otherCell = set.remove(0);
				//if the cell is in a room or above a room, than skip that cell
				if (floorplan.isInRoom(otherCell.getX(), otherCell.getY())
						|| floorplan.isInRoom(otherCell.getX(), otherCell.getY() + 1)) {
					continue;
				}
				wallboard.setLocationDirection(otherCell.getX(), otherCell.getY(), CardinalDirection.South);
				//otherwise, for any cell there's 50% chance of vertical connection, this is to ensure that the maze
				//will have relatively long horizontal path and not too many vertical cut
				int flip = random.nextIntWithinInterval(0, 1);
				if (floorplan.canTearDown(wallboard) && flip > 0) {
					floorplan.deleteWallboard(wallboard);
					connectedCell = new Cell(otherCell.getX(), otherCell.getY()+1);
					newCellToSet.put(connectedCell, setNumber);
					setOfNextRow.add(connectedCell);
				}
			}
			
			
		}
		//Update field reference to a new hashmap
		//containing only the cells of next row
		//no need to look at the current row again
		this.setToCell = newSetToCell;
		this.cellToSet = newCellToSet;
	}
	
	/**
	 * The method horizontally connect all cells in the last row
	 * and merge into one set
	 * if they don't belong to the same set
	 * @param y the index of row to operate on, should only be height-1
	 */
	private void connectAll(int y) {
		
		Wallboard wallboard = new Wallboard(0, 0, CardinalDirection.East);
		
		for (int x = 0; x < width-1; x ++) {
			Cell cell = new Cell(x, y);
			Cell adjacent = new Cell(x+1, y);
			if ((int)cellToSet.get(cell) != (int)cellToSet.get(adjacent)) {
				this.merge(cell, adjacent);
				wallboard.setLocationDirection(x, y, CardinalDirection.East);
				floorplan.deleteWallboard(wallboard);
			}
		}
	}
	
	/**
	 * This helper method add a cells to a new set of its own, and constructs the map between set and cell
	 * @param cell the cell to be added
	 */
	private void addNewSet(Cell cell) {
		cellToSet.put(cell, nextSetNumber);
		List<Cell> cells = new ArrayList<Cell>();
		setToCell.put(nextSetNumber, cells);
		setToCell.get(nextSetNumber).add(cell);
		nextSetNumber++;
	}
	
	/**
	 * This source method merge the set of the sourceCell with the set of its targetCell
	 * by remapping every cell in the source set to the target set, and removed the source set
	 * @param sourceCell a cell to merge into another set
	 * @param targetCell the cell whose set is to be merged into
	 */
	private void merge(Cell sourceCell, Cell targetCell) {
		Integer sourceSetNumber = cellToSet.get(sourceCell);
		Integer targetSetNumber = cellToSet.get(targetCell);
		List<Cell> targetSet = setToCell.get(targetSetNumber);
		List<Cell> sourceSet = setToCell.get(sourceSetNumber);
		for (Cell cell: sourceSet) {
			cellToSet.put(cell, targetSetNumber);
			targetSet.add(cell);
		}
		setToCell.remove(sourceSetNumber);
	}
	
	/**
	 * This class is an abstraction of cells in a puzzle floorplan board
	 * @author yangchen
	 *
	 */
	private class Cell{
		/**
		 * The first number of a cell, should be the column index of the position in the maze board
		 */
		private int x;
		/**
		 * The second number of a cell, should be the row index of the position in the maze board
		 */
		private int y;
		
		public Cell(int x, int y) {
			this.x = x;
			this.y = y;
		}
		/**
		 * @return get the first number of the cell, should be the column index of the cell in the maze board
		 */
		public int getX() {
			return x;
		}
		/**
		 * @return get the second number of the cell, should be the row number of the cell in the maze board
		 */
		public int getY() {
			return y;
		}
		
		@Override
		/**
		 * override the equals method to make sure that if two cells have the same x and the same y,
		 * then they're deemed as equal and should be able to get the same value from the hashmap
		 */
		public boolean equals(Object other) {
			if (this == other)
				return true;
			if (other == null)
				return false;
			if (this.getClass() != other.getClass())
				return false;
			Cell obj = (Cell) other;
			return (this.getX() == obj.getX())&&(this.getY() == obj.getY());
		}
		
		@Override
		public int hashCode() {
			return this.getX() * 2 + this.getY() * 4;
		}
	}
	

	
}
