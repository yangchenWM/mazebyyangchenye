package generation;

import java.util.ArrayList;

/**
 * this class provides a disjoint set data structure to assist maze generation algorithms
 * an object of dijoint set is constructed by DisjSet(int numOfElement) indicating the number of
 * element, which in the maze case should be width * height
 * 
 * each cell (x, y) is mapped to the internal array by index x + y * width
 * 
 * a disjoint set offers two functionalities:1. check to see if two cells are of the same set
 * 2. merge two set
 * 
 * @author yangchen
 *
 */
public class Disjset {
	
	/**
	 * an internal array to store the representation of the tree-like set
	 */
	private int[] sets;
	
	/**
	 *initialize sets to be an array of given size, and set all cells to contain -1, indicating
	 *it is a root in itself
	 * @param numOfElement
	 */
	public Disjset(int numOfElement) {
		sets = new int[numOfElement];
		for (int i = 0; i < sets.length; ++i) {
			sets[i] = -1;
		}
	}
	
	/**
	 * Find which set the cell in the index belongs to
	 * @param index
	 * @return the root of the tree the index cell is in
	 */
	public int find(int index) {
		assert(0 <= index);
		assert(index < sets.length);
		//negative indicates it is the root itself
		if (sets[index] < 0) 
			return index;
		//nonnegative indicates that the number in the indexed cell is its parent,
		//recursively find it's parent's root
		else
			return this.find(sets[index]);
	}
	
	/**
	 * this method joins two sets using height-first union algorithm
	 * note: index1 and index2 has to be root
	 * this method should be conbined with the find method that finds the root
	 * @param index1 the root of the first set
	 * @param index2 the root of the second set
	 */
	public void join(int index1, int index2) {
		int root1 = this.find(index1);
		int root2 = this.find(index2);
		//tree2 is deeper
		if (sets[root2] < sets[root1]) {
			sets[root1] = root2;
		}
		else {
			//if the same height, update the height by one
			if (sets[root1] == sets[root2])
				--sets[root1];
			sets[root2] = root1;
		}
	}
}
