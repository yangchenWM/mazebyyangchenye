package gui;

import gui.Robot.Direction;

/**
 * This class encapsulates wizard's mode-specific behavior of identifying distance with all its sensors
 * 
 * @author yangchen
 *
 */
public class WizardModeAllSensor implements WizardMode {
	
	private Robot robot;
	private RobotDriver wizard;
	

	@Override
	public void setRobotAndDriver(Robot robot, RobotDriver driver) {
		
		this.robot = robot;
		this.wizard = driver;
		
	}
	
	@Override
	public boolean updateDistance(int[] distance) {
		
		try {
			
			distance[0] = robot.distanceToObstacle(Direction.FORWARD);
			distance[1] = robot.distanceToObstacle(Direction.LEFT);
			distance[2] = robot.distanceToObstacle(Direction.RIGHT);
			distance[3] = robot.distanceToObstacle(Direction.BACKWARD);
			
			return true;
			
		} catch(UnsupportedOperationException e) {
			wizard.triggerUpdateSensorInformation();
			return false;
		}

	}



}
