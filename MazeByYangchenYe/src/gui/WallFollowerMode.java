package gui;

/**
 * 
 * This interface encapsulates the mode-specific functionality of different wall
 * follower strategies depening on which sensor is operational in the given robot.
 * The functionalities include different strategies of finding out the distance
 * to the wall in each direction, and also a guiding startegy of escaping the maze
 * (go left or right).
 * 
 * @author yangchen
 *
 */
public interface WallFollowerMode {
	
	/**
	 * set a reference to the robot that a specific mode can work with
	 * @param robot
	 */
	void setRobot(Robot robot);
	
	/**
	 * set a reference to the robot driver that calls the state
	 * @param driver
	 */
	void setDriver(RobotDriver driver);
	
	/**
	 * this method finds out the distance to the forward wall, which can be differently
	 * implemented by different specific modes that instantiate this interface
	 * @return the distance to the forward wall
	 * @throws AssertionError if the game somwhow ended in the way
	 * @throws UnsupportedOperationException if the mode has changed so that the sensor
	 * it relies on is no longer working
	 */
	int findOutForwardDistance() throws AssertionError, UnsupportedOperationException;
	
	/**
	 * this method finds out the distance to the left wall, which can be differently
	 * implemented by different specific modes that instantiate this interface
	 * @return the distance to the forward wall
	 * @throws AssertionError if the game somwhow ended in the way
	 * @throws UnsupportedOperationException if the mode has changed so that the sensor
	 * it relies on is no longer working
	 */
	int findOutLeftDistance() throws AssertionError, UnsupportedOperationException;
	
	/**
	 * this method finds out the distance to the right wall, which can be differently
	 * implemented by different specific modes that instantiate this interface
	 * @return the distance to the forward wall
	 * @throws AssertionError if the game somwhow ended in the way
	 * @throws UnsupportedOperationException if the mode has changed so that the sensor
	 * it relies on is no longer working
	 */
	int findOutRightDistance() throws AssertionError, UnsupportedOperationException;
	
	/**
	 * This method takes the robot to progress one step using the mode-specific strategy,
	 * if that is not possible, it throws exception to make the wallfollower class aware 
	 * of the change and deal with it
	 * @throws AssertionError if the game somwhow ended in the way
	 * @throws UnsupportedOperationException if the mode has changed so that the sensor
	 * it relies on is no longer working
	 */
	void progressWithStrategy() throws AssertionError, UnsupportedOperationException;
	
}
