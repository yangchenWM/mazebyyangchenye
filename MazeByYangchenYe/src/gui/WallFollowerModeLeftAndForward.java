package gui;

import gui.Robot.Direction;
import gui.Robot.Turn;

/**
 * 
 * This class instantiate the WallFollowerMode interface with the mode where the robot's left sensor
 * and forward sensor are both intact
 * In this case, the overall strategy is to turn left whenever it can and directly use the robot's 
 * left and forward sensor to implement this strategy
 * 
 * @author yangchen
 *
 */
public class WallFollowerModeLeftAndForward implements WallFollowerMode {

	Robot robot;
	RobotDriver driver;
	
	@Override
	public void setRobot(Robot robot) {
		this.robot = robot;
	}
	
	@Override
	public void setDriver(RobotDriver driver) {
		this.driver = driver;
	}


	@Override
	public int findOutForwardDistance() throws AssertionError, UnsupportedOperationException {
		//possible to throw exception due to the operation on a different thread
		//handled together later
		return robot.distanceToObstacle(Direction.FORWARD);
	}

	@Override
	public int findOutLeftDistance() throws AssertionError, UnsupportedOperationException {
		//possible to throw exception due to the operation on a different thread
		//handled together later
		return robot.distanceToObstacle(Direction.LEFT);
	
	}

	@Override
	public int findOutRightDistance() throws AssertionError, UnsupportedOperationException {
		//in this mode this method is not used
		throw new RuntimeException("not implemented");
	}

	@Override
	public void progressWithStrategy() throws AssertionError, UnsupportedOperationException {
		
		try {
			int leftDistance = findOutLeftDistance();
			int forwardDistance = findOutForwardDistance();
			
			//if not possible either to go left or to go forward, turn right
			if (leftDistance == 0 && forwardDistance == 0) {
				robot.rotate(Turn.RIGHT);
			}
			else if (leftDistance == 0) { //possible to go forward
				robot.move(1);
			}
			else {//possible to go left
				robot.rotate(Turn.LEFT);
				robot.move(1);
			}
		} catch(UnsupportedOperationException e) { //if the sensor failed and this mode is no longer feasible, trigger
													//the driver to switch mode
			driver.triggerUpdateSensorInformation();
		}
		
	}

}
