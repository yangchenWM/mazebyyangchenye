package gui;

import gui.Robot.Direction;
import gui.Robot.Turn;

/**
 * 
 * This class instantiates the WallFollowerMode interface in the most horrible case where the only sensor
 * that is operational is the backward sensor (if any other sensor is available, there are better modes to use)
 * In the case where we can only use the backward sensor, the general strategy is left-turn. To determine the left
 * distance, the robot should first rotate right, then use its left sensor, and then rotate back left.
 * To determine the forward distance, the robot will use the strategy of move toward the wall once and see if the
 * current position is updated
 * 
 * @author yangchen
 *
 */
public class WallFollowerModeOnlyBack implements WallFollowerMode {
	
	private Robot robot;
	private RobotDriver driver;
	
	@Override
	public void setRobot(Robot robot) {
		this.robot = robot;

	}

	@Override
	public void setDriver(RobotDriver driver) {
		this.driver = driver;

	}

	@Override
	//will not be used in this mode
	public int findOutForwardDistance() throws AssertionError, UnsupportedOperationException {
		throw new RuntimeException("not implemented");
	}

	@Override
	public int findOutLeftDistance() throws AssertionError, UnsupportedOperationException {
		robot.rotate(Turn.RIGHT);
		int result = robot.distanceToObstacle(Direction.BACKWARD);
		robot.rotate(Turn.LEFT);
		return result;
	}

	@Override
	//will not be used in this mode
	public int findOutRightDistance() throws AssertionError, UnsupportedOperationException {
		throw new RuntimeException("not implemented");
	}

	@Override
	public void progressWithStrategy() throws AssertionError, UnsupportedOperationException {
		
		try {
			int leftDistance = findOutLeftDistance();
			if (leftDistance == 0) {//not possible to go left
				int[] curPos = robot.getCurrentPosition();
				robot.move(1); //try move one step
				int[] newPos = robot.getCurrentPosition();
				//there is a wall in front, turn right
				if (curPos[0] == newPos[0] && curPos[1] == newPos[1]) robot.rotate(Turn.RIGHT);
			}
			else {//possible to go left
				robot.rotate(Turn.LEFT);
				robot.move(1);
			}    
		} catch(UnsupportedOperationException e) { //mode failed, trigger the driver to deal with it
			driver.triggerUpdateSensorInformation();
		}
		
	}

}
