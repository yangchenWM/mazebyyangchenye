package gui;

/**
 * 
 * This class has the responsibility to drive the robot to escape the maze by a modified
 * wizard algorithm. Instead of deciding which cell to go by distance value, this class decides
 * according to the menhatten distance between neighbour cells and the exit position. The rest are
 * the same as the wizard
 * 
 * @author yangchen
 *
 */
public class WizardByGeometricDistance extends Wizard {

	
	
}
