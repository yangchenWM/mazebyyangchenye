package gui;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * This class displays on the playing screen possible user choices and deals with user input at the playing screen
 * 
 * @author yangchen
 *
 */
public class SensorPanel extends JPanel{

	JButton btLeft;
	JButton btRight;
	JButton btForward;
	JButton btBackward;
	
	Controller controller;
	
	public SensorPanel(Controller controller) {
		this.controller = controller;
	}
	
	public void statePlayingInit() {
		
		this.setBackground(Color.cyan);
		this.setLayout(new GridLayout(1, 4));
		
		this.addKeyListener(new SimpleKeyListener(this, controller));
		
		ActionListener al = controller;
		
		//declare four button
		JButton btLeft;
		JButton btRight;
		JButton btForward;
		JButton btBackward;
		
		btLeft = new JButton("Left");
		btLeft.addActionListener(al);
		btLeft.addKeyListener(new SimpleKeyListener(this, controller));
		
		add(btLeft);
		
		btRight = new JButton("Right");
		btRight.addActionListener(al);
		btRight.addKeyListener(new SimpleKeyListener(this, controller));
		
		add(btRight);
		
		btForward = new JButton("Forward");
		btForward.addActionListener(al);
		btForward.addKeyListener(new SimpleKeyListener(this, controller));
		
		add(btForward);
		
		btBackward = new JButton("Backward");
		btBackward.addActionListener(al);
		btBackward.addKeyListener(new SimpleKeyListener(this, controller));
		
		add(btBackward);
		
	}


}
