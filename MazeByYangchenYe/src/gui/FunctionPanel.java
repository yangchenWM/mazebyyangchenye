package gui;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;


/**
 * This class provides functionality of GUI user choices in the stateTitle
 * In the statePlaying, three comboboxes, builder driver and skill level, and
 * a starting button is displayed, and the user can select which builder, driver and
 * skill level they want.
 * 
 * 
 * @author yangchen
 *
 */
public class FunctionPanel extends JPanel {
	
	private Controller controller;
	
	public FunctionPanel(Controller controller) {
		super();
		this.controller = controller;
		this.init();
		this.setBackground(Color.cyan);
	}
	
	public void init() {
		
		
		this.setLayout(new GridLayout(1, 4));
		
		ActionListener al = new SimpleActionListener(this, controller);
		
		String[] builders = {"DFS", "Prim", "Eller", "Kruskal", "HAK"};
		JComboBox<String> cb = new JComboBox<String>(builders);
		
		//combobox of builders
		add(cb);
		cb.addActionListener(al);
		
		String[] drivers = {"Manual", "Wizard", "Explorer", "WallFollower"};
		
		JComboBox<String> cd = new JComboBox<String>(drivers);
		
		//combobox of drivers
		add(cd);
		cd.addActionListener(al);
	
		Integer[] skills = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
		
		JComboBox<Integer> cs = new JComboBox<Integer>(skills);
		
		cs.addActionListener(al);
		
		add(cs);
		
		JButton bt = new JButton("start");
		
		bt.addActionListener(al);
		
		
		add(bt);
		
		
	}
	
	
}
