package gui;

import gui.Robot.Direction;
import gui.Robot.Turn;

/**
 * 
 * This class instantiates the WallFollowerMode interface in the case where the robot has
 * no left sensor or forward sensor, but has a right sensor.
 *
 * 
 * @author yangchen
 *
 */
public class WallFollowerModeOnlyRight implements WallFollowerMode {
	
	private Robot robot;
	private RobotDriver driver;
	
	@Override
	public void setRobot(Robot robot) {
		this.robot = robot;

	}

	@Override
	public void setDriver(RobotDriver driver) {
		this.driver = driver;

	}

	@Override
	public int findOutForwardDistance() throws AssertionError, UnsupportedOperationException {
		throw new RuntimeException("not implemented");
	}

	@Override
	//will not be usde in this mode
	public int findOutLeftDistance() throws AssertionError, UnsupportedOperationException {
		robot.rotate(Turn.AROUND);
		int result = robot.distanceToObstacle(Direction.RIGHT);
		robot.rotate(Turn.AROUND);
		return result;
	}

	@Override
	public int findOutRightDistance() throws AssertionError, UnsupportedOperationException {
		throw new RuntimeException("not implemented");
	}

	@Override
	public void progressWithStrategy() throws AssertionError, UnsupportedOperationException {
		try {
			int[] currentPosition = robot.getCurrentPosition();
			int leftDistance = findOutLeftDistance();
			if (leftDistance == 0) { //possible to go forward or turn right
				robot.move(1);
				int[] newPosition = robot.getCurrentPosition();
				//check to see if the move suceeded, if not, turn right
				if (newPosition[0] == currentPosition[0] && newPosition[1] == currentPosition[1])
					robot.rotate(Turn.RIGHT);
			}
			else { //possible to go left
				robot.rotate(Turn.LEFT);
				robot.move(1);
			}
		} catch(UnsupportedOperationException e) { //the sensor failed and mode unfeasible, handle control to the
													//driver to deal with
			driver.triggerUpdateSensorInformation();
		}

	}

}
