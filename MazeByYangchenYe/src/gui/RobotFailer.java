package gui;

import gui.Robot.Direction;

/**
 * This class encapsulates the functionality of triggering robot's sensor failure and repairing failed
 * sensor in a different thread with a time interval of 3 second
 * 
 * @author yangchen
 *
 */
public class RobotFailer implements Runnable {
	
	private Controller controller;
	private Robot robot;
	private Direction d;
	
	public RobotFailer(Controller controller, Direction d) {
		this.controller = controller;
		this.robot = controller.getRobot();
		this.d = d;
	}
	
	@Override
	public void run() {
		
		while (controller.isPlayable()) {
			
			if (robot.hasOperationalSensor(d))
				robot.triggerSensorFailure(d);
			else
				robot.repairFailedSensor(d);
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				robot.repairFailedSensor(d);
				return;
			}
			
		}

	}

}
