package gui;

import gui.Robot.Direction;
import gui.Robot.Turn;

/**
 * This class encapsulates wizard's mode-specific behavior of identifying distances with both forward and backward sensor.
 * 
 * @author yangchen
 *
 */
public class WizardModeLeftAndRight implements WizardMode {
	
	private Robot robot;
	private RobotDriver wizard;

	@Override
	public void setRobotAndDriver(Robot robot, RobotDriver driver) {
		this.robot = robot;
		this.wizard = driver;

	}

	@Override
	public boolean updateDistance(int[] distance) {
		
		try {
			distance[1] = robot.distanceToObstacle(Direction.LEFT);
			distance[2] = robot.distanceToObstacle(Direction.RIGHT);
		} catch (UnsupportedOperationException e) {
			wizard.triggerUpdateSensorInformation();
			return false;
		}
		
		robot.rotate(Turn.LEFT);
		
		try {
			distance[3] = robot.distanceToObstacle(Direction.LEFT);
			distance[0] = robot.distanceToObstacle(Direction.RIGHT);
		} catch(UnsupportedOperationException e) {
			robot.rotate(Turn.RIGHT);
			wizard.triggerUpdateSensorInformation();
			return false;
		}
		
		robot.rotate(Turn.RIGHT);
		return true;
		
	}

}
