package gui;

import generation.CardinalDirection;
import generation.Maze;
import generation.Wallboard;
import gui.Constants.UserInput;


/**
 * This class has the responsibility to implement a robot interface, which provides a full
 * set of operations to manipulate the maze controller and play the game. The functionalities that
 * should be supported can be classified into 3 types:
 * 
 * 1. The robot API should provide actuator methods to move, jump and rotate in the maze, so that a
 * further maze-solving algorithm can call these methods to automatically travel the maze. These set
 * of methods are implemented by simulating User Input and pass the input to the controller, so that the 
 * effect of calling an atuator method in this class resembles that of pressing a keyboard buttom when playing
 * the game.
 * Collaborators: controller, stateplaying(through controller)
 * Upstream client: RobotDriver, maze solving algorithms
 * 
 * 2. The robot API should provide sensor methods which gathers and returns information about its state inside the maze,
 * which includes: the current direction and position; whether it is currently at the exit cell, or is inside a room; whether
 * the exit can be seen in the current direction; and how far it is to get to the exit. These methods are implemented with
 * the collaboration of the controller, the statePlaying, and finally the maze field stored in that state.
 * Collaborators: controller, stateplaying(through controller).
 * Collaborators: controller, stateplaying(through controller)
 * Upstream client: RobotDriver, maze solving algorithms
 * 
 * 3. The robot API also maintains an internal state, Battery level, and a series of method manipulating this internal state.
 * It is initialized to a specified value; each operation would consume energy and update that state; if there is no enough energy,
 * an operation would not be possible; and finally, if the battery level is down to zero, the robot cannot move and the game is failed.
 * Besides Battery level, the robot keeps track of the total distance it has traveled, which should be updated accordingly after each
 * move or jump.
 * 
 * 
 * @author yangchen
 *
 */
public class BasicRobot implements Robot {
	
	static final int ENERGY_FOR_SENSING = 1;
	static final int ENERGY_FOR_FULL_ROTATION = 12;
	static final int ENERGY_FOR_STEP_FORWARD = 5;
	static final int ENERGY_FOR_JUMP = 50;
	static final float INITIAL_BATTERY_LEVEL = 3000;
	static final int INFINITY = Integer.MAX_VALUE;
	
	/**
	 * keep a reference to the controller object this robot is collaborating with
	 */
	private Controller controller;
	
	/**
	 * the internal state, battery level, of the robot
	 */
	private float batteryLevel;
	
	/**
	 * the internal state, the total distance the robot has passed
	 */
	private int odoMeter;
	
	/**
	 * keep a reference to the maze the robot is working on to assist several methods
	 */
	private Maze maze;
	
	//Four Flags whether Directional Signal is operational//
	
	private boolean leftSensorOperational = true;
	private boolean rightSensorOperational = true;
	private boolean forwardSensorOperational = true;
	private boolean backwardSensorOperational = true;
	
	/**
	 * default constructor sets fields by default value
	 */
	public BasicRobot() {
		setBatteryLevel(INITIAL_BATTERY_LEVEL);
		odoMeter = 0;
	}
	
	/**
	 * customized constructor allowing self-defined battery level
	 * @param batteryLevel customized initial battery level
	 */
	public BasicRobot(float batteryLevel) {
		this();
		setBatteryLevel(batteryLevel);
	}

	@Override
	/**
	 * set a reference to the controller to work with
	 * warning: this function is not expected to be called manually,
	 * the controller will call it at the right time
	 */
	public void setMaze(Controller controller) {
		//precondition: controller cannot be null and the maze has to be delivered
		assert null != controller;
		this.controller = controller;
		setMazeConfig();
	}
	
	/**
	 * this method sets a reference to the delivered maze
	 */
	public void setMazeConfig() {
		//precondition: controller cannot be null and the maze has to be delivered
		checkPreconditionOnController();
		this.maze = controller.getMazeConfiguration();
	}
	
	/**
	 * check pre-condition for a valid operation of robot:
	 * 1. there has to be a controller to work with
	 * 2. the controller must be in a state possible to play
	 */
	private void checkPreconditionOnController() {
		assert null != controller;
		assert controller.isPlayable();
	}
	
	///////////////////////////////////////Sensor Methods//////////////////////////////////////////////////////
	
	@Override
	/**
	 * return the current position of the robot
	 * decide not to throw exception because there is no
	 * possibility that the controller shall return an invalid position
	 */
	public int[] getCurrentPosition(){
		checkPreconditionOnController();
		
		return controller.getCurrentPosition();
	}

	@Override
	public CardinalDirection getCurrentDirection() {
		checkPreconditionOnController();
		
		return controller.getCurrentDirection();
	}
	
	@Override
	//no exception is thrown in this implementation
	public boolean canSeeThroughTheExitIntoEternity(Direction direction) throws UnsupportedOperationException{
		checkPreconditionOnController();
		
		//consume energy
		batteryLevel -= ENERGY_FOR_SENSING;
		controller.addEnergyConsumption(ENERGY_FOR_SENSING);
		
		int[] curPosition = this.getCurrentPosition();
		
		CardinalDirection objectiveDirection = this.subToObj(direction);
		
		//call the private method so that it will bypass the disabld sensor signal and give the result
		int distance = this.distanceToObstacle(curPosition[0], curPosition[1], objectiveDirection);
		
		//distance to obstacle in the given direction is INFINITY just means there is an
		//exit in this direction
		if (distance == INFINITY) return true;
		
		return false; 
	}

	
	@Override
	public boolean isAtExit() {
		checkPreconditionOnController();
		
		int x, y;
		
		x = this.getCurrentPosition()[0];
		y = this.getCurrentPosition()[1];
		
		int[] exit = maze.getMazedists().getExitPosition();
		
		return (x == exit[0] && y == exit[1]);

	}
	@Override
	public boolean isInsideRoom() throws UnsupportedOperationException {
		checkPreconditionOnController();
		
		int x, y;
		x = this.getCurrentPosition()[0];
		y = this.getCurrentPosition()[1];
		
		return maze.getFloorplan().isInRoom(x, y);
	}
	
	@Override
	public int distanceToObstacle(Direction direction) throws UnsupportedOperationException {
		checkPreconditionOnController();
		
		//if sensor is broken, throw an exception and reject the task
		if (!this.hasOperationalSensor(direction)) throw new UnsupportedOperationException("Sensor Broken");
		
		//sensor intact, do the work
		//consume energy
		controller.addEnergyConsumption(ENERGY_FOR_SENSING);
		batteryLevel -= ENERGY_FOR_SENSING;
		
		int x, y;
		
		x = this.getCurrentPosition()[0];
		y = this.getCurrentPosition()[1];
		
		//calculate the objective cardinal direction with the given direction and the current direction of the robot
		CardinalDirection objectiveDirection = this.subToObj(direction);
		
		//hand the task over to an accompanying method based on objective cardinal direction
		int distance = this.distanceToObstacle(x, y, objectiveDirection);
		
		//pass the value to the state to be displayed on the screen
		StatePlaying play = (StatePlaying) controller.currentState;
		play.setSensor(distance);
		
		//key down an buttom to trigger the statePlaying class to update the screen
		switch(direction) {
		case FORWARD:
			controller.currentState.keyDown(UserInput.ForwardDistanceSensor, 0);
			break;
		case LEFT:
			controller.currentState.keyDown(UserInput.LeftDistanceSensor, 0);
			break;
		case RIGHT:
			controller.currentState.keyDown(UserInput.RightDistanceSensor, 0);
			break;
		case BACKWARD:
			controller.currentState.keyDown(UserInput.BackwardDistanceSensor, 0);
			break;
					
		}
		
		return distance;
		
		
	}
	
	/**
	 * this helper method convert the direction relative to the robot's current location to an objecive cardinal
	 * direction
	 * @param direction subjective direction: left, right, forward, backward
	 * @return cardinal direction: east, west, south, north
	 */
	private CardinalDirection subToObj(Direction direction) {
		
		//calculate the objective cardinal direction with the given direction and the current direction of the robot
		CardinalDirection objectiveDirection = CardinalDirection.East;
				
		switch(direction) {
			case FORWARD:
				objectiveDirection = this.getCurrentDirection();
				break;
			case LEFT:
				objectiveDirection = this.getCurrentDirection().rotateClockwise();
				break;
			case BACKWARD:
				objectiveDirection = this.getCurrentDirection().oppositeDirection();
				break;
			case RIGHT:
				CardinalDirection im = this.getCurrentDirection().oppositeDirection();
				objectiveDirection = im.rotateClockwise();
				break;
		}
		
		return objectiveDirection;
					
	}
	
	/**
	 * private helper method. calculates the distance from the given start position to the nearest wall along the
	 * given cardinal direction. return INFINITY if it goes right out of the exit along that direction.
	 * @param startx
	 * @param starty
	 * @param oD
	 * @return
	 */
	private int distanceToObstacle(int startx, int starty, CardinalDirection oD) {
		
		//set up direction
		int[] dir = oD.getDirection();
		int dx = dir[0];
		int dy = dir[1];
		
		//initialize counter
		int distCount = 0;
		//move along the given direction till it hit a wall or goes out from the exit
		while (true) {
			
			//if successfully goes out of the maze without hitting a wall, the distanceis inifnity;
			if (!maze.isValidPosition(startx, starty)) return INFINITY;
			
			//if hit a wall, break out of the loop and return the value stored in the current counter
			if (maze.getFloorplan().hasWall(startx, starty, oD)) break;
			
			//otherwise, go one step further and increment the counter
			startx += dx;
			starty += dy;
			++distCount;
		}
		
		
		return distCount;
	}

	///////////////////////////////////////Actuator methods////////////////////////////////////////////////
	@Override
	/**
	 * this method drives the robot to turn to the specified direction
	 */
	public void rotate(Turn turn) {
		checkPreconditionOnController();
		
		//update internal state and press the according buttom to trigger screen update
		switch(turn) {
		case LEFT:
			controller.addEnergyConsumption(ENERGY_FOR_FULL_ROTATION/4);
			batteryLevel -= ENERGY_FOR_FULL_ROTATION/4;
			controller.currentState.keyDown(UserInput.Left, 0);
			break;
		case RIGHT:
			controller.addEnergyConsumption(ENERGY_FOR_FULL_ROTATION/4);
			batteryLevel -= ENERGY_FOR_FULL_ROTATION/4;
			controller.currentState.keyDown(UserInput.Right, 0);
			break;
		case AROUND:
			controller.addEnergyConsumption(ENERGY_FOR_FULL_ROTATION/2);
			batteryLevel -= ENERGY_FOR_FULL_ROTATION/2;
			controller.currentState.keyDown(UserInput.Right, 0);
			controller.currentState.keyDown(UserInput.Right, 0);
			break;
		default: //no use
			break;
			
		}
		            
	}

	@Override
	public void move(int distance) {
		checkPreconditionOnController();
		
		//pre-condition: distance >= 0
		assert distance >= 0;
		
		//do consecutive walks
		for (int i = 0; i < distance; ++ i)
			this.walk();
		
		//post-condition: the position is still valid
		//no need to check because of this specific
		//implementation

	}
	
	/**
	 * walk one step in the current direction. A move operation can be broke down
	 * into consecutive walks
	 */
	private void walk() {

		checkPreconditionOnController();
		
		//the position before walk
		int startx = this.getCurrentPosition()[0];
		int starty = this.getCurrentPosition()[1];
		CardinalDirection cd = this.getCurrentDirection();
		
		//if not has wall, the walk is going to succeed and odometer is updated
		if (!maze.getFloorplan().hasWall(startx, starty, cd)) ++odoMeter;
		
		//consumes energy
		controller.addEnergyConsumption(ENERGY_FOR_STEP_FORWARD);
		batteryLevel -= ENERGY_FOR_STEP_FORWARD;
		
		controller.currentState.keyDown(UserInput.Up, 0);
			
	}

	@Override
	/**
	 * walk one cell forward regardless whether there is a wallboard in front,
	 * should have no effect if the jump goes out of bound
	 * decide not to throw execption due to this specific implementation
	 */
	public void jump(){
		checkPreconditionOnController();
		
		
		//the position before jump
		int startx = this.getCurrentPosition()[0];
		int starty = this.getCurrentPosition()[1];
		CardinalDirection cd = this.getCurrentDirection();
		Wallboard wallToJump = new Wallboard(startx, starty, cd);
		int neighbourX = wallToJump.getNeighborX();
		int neighbourY = wallToJump.getNeighborY();
		
		//if the jump is going to fail, return and nothing is updated
		if (maze.isValidPosition(neighbourX, neighbourY)) ++odoMeter;
		
		//consume energy and update odometer
		controller.addEnergyConsumption(ENERGY_FOR_JUMP);
		batteryLevel -= ENERGY_FOR_JUMP;
		
		controller.currentState.keyDown(UserInput.Jump, 0);
		
		
		//post-condition: the position is still valid
		//no need to test given this specific implementation

	}
	
	/////////////////////////////////////Internal State Manipulation///////////////////////////////////////
	
	@Override
	public float getBatteryLevel() {
		return this.batteryLevel;
	}

	@Override
	public void setBatteryLevel(float level) {
		this.batteryLevel = level;
		
		//if in the playing state, triggr end-game checking
		if (null != controller && controller.isPlayable())
			controller.currentState.keyDown(UserInput.Start, 0);

	}

	@Override
	public int getOdometerReading() {
		return this.odoMeter;
	}

	@Override
	public void resetOdometer() {
		this.odoMeter = 0;

	}

	@Override
	public float getEnergyForFullRotation() {
		return ENERGY_FOR_FULL_ROTATION;
	}

	@Override
	public float getEnergyForStepForward() {
		return ENERGY_FOR_STEP_FORWARD;
	}

	@Override
	public boolean hasRoomSensor() {
		return true;
	}

	@Override
	public boolean hasStopped() {
		//if battery level goes below 0, stop
		if (this.batteryLevel <= 0) return true;
		return false;
	}

	@Override
	public boolean hasOperationalSensor(Direction direction) {
		switch(direction) {
		case FORWARD:
			return this.forwardSensorOperational;
		case LEFT:
			return this.leftSensorOperational;
		case RIGHT:
			return this.rightSensorOperational;
		case BACKWARD: default:
			return this.backwardSensorOperational;
			
		}
	}

	@Override
	public void triggerSensorFailure(Direction direction) {
		switch(direction) {
		case FORWARD:
			this.forwardSensorOperational = false;
			break;
		case LEFT:
			this.leftSensorOperational = false;
			break;
		case RIGHT:
			this.rightSensorOperational = false;
			break;
		case BACKWARD:
			this.backwardSensorOperational = false;
			break;
		}
		
		//if operation done in the playing screen, update the screen
		if (controller != null && controller.isPlayable())
			controller.currentState.keyDown(UserInput.FailSensor, 0);
	}

	@Override
	public boolean repairFailedSensor(Direction direction) {
		switch(direction) {
		case FORWARD:
			this.forwardSensorOperational = true;
			break;
		case LEFT:
			this.leftSensorOperational = true;
			break;
		case RIGHT:
			this.rightSensorOperational = true;
			break;
		case BACKWARD:
			this.backwardSensorOperational = true;
			break;
		}
		
		//if operation done in the playing screen, update the screen
		if (controller.isPlayable())
			controller.currentState.keyDown(UserInput.RepairSensor, 0);
		return true;
	}

}
