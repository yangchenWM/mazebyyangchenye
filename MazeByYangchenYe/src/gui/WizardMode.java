package gui;

/**
 * Encapsulates the mode-specific behavior of a wizard in updating distance information with specific
 * sensor setting
 * 
 * @author yangchen
 *
 */
public interface WizardMode {
	
	/**
	 * Set the robot and driver for the mode to work with
	 * @param robot
	 * @param driver
	 */
	void setRobotAndDriver(Robot robot, RobotDriver driver);
	
	/**
	 * update the distance information based on robot and driver
	 * @param distance
	 * @return true if update is successful, false otherwise
	 */
	boolean updateDistance(int[] distance);
	
}
