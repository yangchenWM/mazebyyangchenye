package gui;

import javax.management.RuntimeErrorException;

import generation.Distance;

/**
 * This class inplements the robot driver interface, which has the responsibility to
 * provides functionality of a robot driver that drives a robot to escape from the maze
 * Specific robot driver classes should extend this default class and override the 
 * driving method only
 * 
 * 
 * @author yangchen
 *
 */
public class DefaultRobotDriver implements RobotDriver, Runnable {

	boolean started;
	
	/**
	 * the robot which this driver operarates on
	 */
	protected Robot robot;
	
	/**
	 * width of the maze to play with
	 */
	protected int width;
	
	/**
	 * height of the maze to play with
	 */
	protected int height;
	
	/**
	 * the initial battery level of the robot
	 */
	protected float initialBatteryLevel;
	
	/**
	 * distance matrix of the maze
	 */
	protected Distance distance;
	
	public DefaultRobotDriver() {
		started = false;
	}

	@Override
	public void setRobot(Robot r) {
		//pre-condition robot cannot be null
		assert r != null;
		started = true;
		robot = r;
		initialBatteryLevel = robot.getBatteryLevel();
		
	}

	@Override
	public void setDimensions(int width, int height) {
		this.width = width;
		this.height = height;

	}

	@Override
	public void setDistance(Distance distance) {
		this.distance = distance;

	}

	@Override
	//wait to be implemented by each particular robot driver
	//behavior depends on each class
	public void triggerUpdateSensorInformation() {
		throw new RuntimeErrorException(null, "not implemented");
	}

	@Override
	//wait to be implemented by each particular robot driver class
	public boolean drive2Exit() throws Exception {
		throw new RuntimeErrorException(null, "not implemented");
	}

	@Override
	public float getEnergyConsumption() {
		assert started;
		
		return initialBatteryLevel - robot.getBatteryLevel();
	}

	@Override
	public int getPathLength() {
		assert started;
		
		return robot.getOdometerReading();
	}

	@Override
	public void run() {
		try {
			drive2Exit();
		} catch(Exception e) {
			//
		}
		
	}

}
