package gui;

import gui.Robot.Direction;
import gui.Robot.Turn;

/**
 * This class encapsulates wizard's mode-specific behavior of identifying distances with a left sensor.
 * 
 * @author yangchen
 *
 */
public class WizardModeRight implements WizardMode {

	private Robot robot;
	private RobotDriver wizard;
	

	@Override
	public void setRobotAndDriver(Robot robot, RobotDriver driver) {
		
		this.robot = robot;
		this.wizard = driver;
		
	}
	
	@Override
	public boolean updateDistance(int[] distance) {
		
		try {
			
			distance[2] = robot.distanceToObstacle(Direction.RIGHT); //right distance
			
		} catch(UnsupportedOperationException e) {
			wizard.triggerUpdateSensorInformation();
			return false;
		}
			
		robot.rotate(Turn.LEFT);
			
		try {
				
			distance[0] = robot.distanceToObstacle(Direction.RIGHT); //forward distance
				
		} catch(UnsupportedOperationException e) {
			robot.rotate(Turn.RIGHT);
			wizard.triggerUpdateSensorInformation();
			return false;
		}
			
		robot.rotate(Turn.LEFT);
		
		try{
			
			distance[1] = robot.distanceToObstacle(Direction.RIGHT); //left distance
			
		} catch (UnsupportedOperationException e) {
			robot.rotate(Turn.AROUND);
			wizard.triggerUpdateSensorInformation();
			return false;
		}
		
		robot.rotate(Turn.LEFT);
		
		try {
			distance[3] = robot.distanceToObstacle(Direction.RIGHT); //backward distance
		} catch (UnsupportedOperationException e) {
			robot.rotate(Turn.LEFT);
			wizard.triggerUpdateSensorInformation();
			return false;
		}
		
		robot.rotate(Turn.LEFT);
			
		
		return true;
			

	}

}
