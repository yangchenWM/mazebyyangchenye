package gui;

import gui.Constants.UserInput;
import gui.Constants.UserMouseInput;
import gui.Robot.Direction;
import gui.Robot.Turn;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import generation.CardinalDirection;
import generation.Maze;
import generation.Order;
import generation.Order.Builder;

import gui.StatePlaying;


/**
 * Class handles the user interaction. 
 * It implements an automaton with states for the different stages of the game.
 * It has state-dependent behavior that controls the display and reacts to key board input from a user. 
 * At this point user keyboard input is first dealt with a key listener (SimpleKeyListener)
 * and then handed over to a Controller object by way of the keyDown method.
 *
 * The class is part of a state pattern. It has a state object to implement
 * state-dependent behavior.
 * The automaton currently has 4 states.
 * StateTitle: the starting state where the user can pick the skill-level
 * StateGenerating: the state in which the factory computes the maze to play
 * and the screen shows a progress bar.
 * StatePlaying: the state in which the user plays the game and
 * the screen shows the first person view and the map view.
 * StateWinning: the finish screen that shows the winning message.
 * The class provides a specific method for each possible state transition,
 * for example switchFromTitleToGenerating contains code to start the maze
 * generation.
 *
 * This code is refactored code from Maze.java by Paul Falstad, 
 * www.falstad.com, Copyright (C) 1998, all rights reserved
 * Paul Falstad granted permission to modify and use code for teaching purposes.
 * Refactored by Peter Kemper
 * 
 * @author Peter Kemper
 */
public class Controller implements ActionListener{
	/**
	 * The game has a reservoir of 4 states: 
	 * 1: show the title screen, wait for user input for skill level
	 * 2: show the generating screen with the progress bar during 
	 * maze generation
	 * 3: show the playing screen, have the user or robot driver
	 * play the game
	 * 4: show the finish screen with the winning/loosing message
	 * The array entries are set in the constructor. 
	 * There is no mutator method.
	 */
    State[] states;
    /**
     * The current state of the controller and the game.
     * All state objects share the same interface and can be
     * operated in the same way, although the behavior is 
     * vastly different.
     * currentState is never null and only updated by 
     * switchFrom .. To .. methods.
     */
    State currentState;
    
    /**
     * Reference to the jframe that is carrying out the game
     */
    JFrame app;
    
    /**
     * The panel is used to draw on the screen for the UI.
     * It can be set to null for dry-running the controller
     * for testing purposes but otherwise panel is never null.
     */
    MazePanel panel;
    
    /**
     * The panel is used to show comboboxes on the UI.
     * In the stateTitle, it shows the comboboxes of builder selection,
     * driver selection and skill level selection and a start button.
     * In the statePlaying, it shows four buttons that triggers four sensor failures
     * or repairment
     */
    FunctionPanel fpanel;
    
    /**
     * The panel is used to show buttons in the playing screen
     */
    SensorPanel spanel;
    
    /**
     * The filename is optional, may be null, and tells
     * if a maze is loaded from this file and not generated.
     */
    String fileName;
    /**
     * The builder algorithm to use for generating a maze.
     */
    Order.Builder builder;
    /**
     * Specifies if the maze is perfect, i.e., it has
     * no loops, which is guaranteed by the absence of 
     * rooms and the way the generation algorithms work.
     */
    boolean perfect;
    
    /**
     * specifies the skill level of the maze, selected by user input
     */
    int skillLevel;
    
    public Controller() {
    	states = new State[4];
        states[0] = new StateTitle();
        states[1] = new StateGenerating();
        states[2] = new StatePlaying();
        states[3] = new StateEnding();
        currentState = states[0];
        //initialize panels for use
        panel = new MazePanel(this); 
        fpanel = new FunctionPanel(this);
        spanel = new SensorPanel(this);
        fileName = null;
        builder = Order.Builder.DFS; // default
        perfect = false; // default
    }
    
    public void setFrame(JFrame app) {
    	this.app = app;
    }
    
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public void setBuilder(Builder builder) {
        this.builder = builder; 
    }
    public void setPerfect(boolean isPerfect) {
        this.perfect = isPerfect; 
    }  
    public void setSkillLevel(int level) {
    	this.skillLevel = level;
    }
    public MazePanel getPanel() {
        return panel;
    }
    public FunctionPanel getFunctionPanel() {
    	return fpanel;
    }
    
    public SensorPanel getSensorPanel() {
    	return spanel;
    }
    
    /**
     * Starts the controller and begins the game 
     * with the title screen.
     */
    public void start() { 
        currentState = states[0]; // initial state is the title state
        currentState.setFileName(fileName); // can be null
        currentState.start(this, panel);
        fileName = null; // reset after use
    }
    /**
     * Switches the controller to the generating screen.
     * Assumes that builder and perfect fields are already set
     * with set methods if default settings are not ok.
     * A maze is generated.
     * @param skillLevel, 0 <= skillLevel, size of maze to be generated
     */
    public void switchFromTitleToGenerating() {
    	//function panel is no longer of use, set it invisible
    	//and hand the focus control to the sensor panel which
    	//will take charge of the user input during the game
    	fpanel.setFocusable(false);
    	fpanel.setVisible(false);
    	spanel.setFocusable(true);
    	spanel.setVisible(true);
    	spanel.grabFocus();
    	if (app != null) {
    		app.revalidate();
    		app.repaint();
    	}
    	
        currentState = states[1];
        currentState.setSkillLevel(skillLevel);
        currentState.setBuilder(builder);  
        currentState.setPerfect(perfect); 
        currentState.start(this, panel);
    }
    /**
     * Switches the controller to the generating screen and
     * loads maze from file.
     * @param filename gives file to load maze from
     */
    public void switchFromTitleToGenerating(String filename) {
    	//function panel is no longer of use, set it invisible
    	//and hand the focus control to the sensor panel which
    	//will take charge of the user input during the game
    	fpanel.setFocusable(false);
    	fpanel.setVisible(false);
    	spanel.setFocusable(true);
    	spanel.setVisible(true);
    	spanel.grabFocus();
    	if (app != null) {
    		app.revalidate();
    		app.repaint();
    	}
    	
        currentState = states[1];
        currentState.setFileName(filename);
        currentState.start(this, panel);
    }
    /**
     * Switches the controller to the playing screen.
     * This is where the user or a robot can navigate through
     * the maze and play the game.
     * @param config contains a maze to play
     */
    public void switchFromGeneratingToPlaying(Maze config) {
    	
    	spanel.revalidate();
    	spanel.repaint();
    	if (app != null)
    		app.repaint();
    	
        currentState = states[2];
        currentState.setMazeConfiguration(config);
        if (robot != null) robot.setMaze(this);
        if (driver != null) { 
        	driver.setRobot(robot);
        	driver.setDimensions(config.getWidth(), config.getHeight());
        	driver.setDistance(config.getMazedists());
        }
        
        
        currentState.start(this, panel);
        
        keyDown(UserInput.ToggleLocalMap, 0);
        keyDown(UserInput.ToggleFullMap, 0);
        keyDown(UserInput.ToggleSolution, 0);
        
        
        if (driver != null) {
				driverThread = new Thread(driver);
				driverThread.start();			
        }

    }
    /**
     * Switches the controller to the final screen
     * @param pathLength gives the length of the path
     * @param if the game is won or lost
     */
    public void switchFromPlayingToEnding(int pathLength, boolean winned) {
    	//clean up thread
    	this.stopAllThread();
    	
    	//in the ending screen, none of the panel should be
    	//visible or focusable
    	fpanel.setFocusable(false);
    	spanel.setFocusable(false);
    	spanel.setVisible(false);
    	if (app != null) {
    		app.setFocusable(true);
    		app.requestFocusInWindow();
    		app.revalidate();
    		app.repaint();
    	}
        currentState = states[3];
        StateEnding endState = (StateEnding) currentState;
        
        //hand in necessary information
        endState.setPathLength(pathLength);
        endState.setWinnedGame(winned); 
        endState.setEnergyConsumed(this.getEnergyConsumption());
    	
    	currentState.start(this, panel);
    }
    /**
     * Switches the controller to the initial screen.
     */
    public void switchToTitle() {
    	//clean up thread
    	this.stopAllThread();
    	
    	//set up panels
    	spanel.setFocusable(false);
    	spanel.setVisible(false);
    	fpanel.setVisible(true);
    	fpanel.setFocusable(true);
    	if (app != null) {
    		app.setFocusable(true);
    		app.revalidate();
    		app.repaint();
    	}
    	
    	//reinitialize energy counter
    	robotEnergyConsumption = 0;
    	
    	//restore robot battery and odometer
    	robot.setBatteryLevel(3000);
    	robot.resetOdometer();
    	
    	//start again
        currentState = states[0];
        
        //sleep a little while to prevent race condition and let the driver thread terminate
        try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        currentState.start(this, panel);
    }
    
    /**
     * Method incorporates all reactions to keyboard input in original code. 
     * The simple key listener calls this method to communicate input.
     */
    public boolean keyDown(UserInput key, int value) {
        // delegated to state object
    	
    	//if there is a robot to operate, hand control to robot
    	if (currentState == states[2] && robot != null) {
    		switch(key) {
    		case Up:
    			robot.move(1);
    			break;
    		case Left:
    			robot.rotate(Turn.LEFT);
    			break;
    		case Right:
    			robot.rotate(Turn.RIGHT);
    			break;
    		case Jump:
    			robot.jump();
    			break;
    		case LeftDistanceSensor:
    			robot.distanceToObstacle(Direction.LEFT);
    			break;
    		case RightDistanceSensor:
    			robot.distanceToObstacle(Direction.RIGHT);
    			break;
    		case ForwardDistanceSensor:
    			robot.distanceToObstacle(Direction.FORWARD);
    			break;
    		case BackwardDistanceSensor:
    			robot.distanceToObstacle(Direction.BACKWARD);
    			break;
    		//not robot operation
    		default:
    			return currentState.keyDown(key, value);
    		}
    		return true;
    	}
    	
    	//else proceed to default operation
    	else return currentState.keyDown(key, value);
    }
    
    /**
     * Method incorporates all reactions to user mouse input in original code. 
     * The simple action listener calls this method to communicate input.
     */
    public boolean actionPerformed(UserMouseInput input, int value) {
    	return currentState.actionPerformed(input, value);
    }
    
    /**
     * Method returns if the controller is now in statePlaying,
     * meaning the maze can be played
     * @return
     */
    public boolean isPlayable() {
    	return currentState == states[2];
    }
    
    /**
     * Method sets current position in the maze
     * @param x
     * @param y
     */
    public void setCurrentPosition(int x, int y) {
    	assert this.isPlayable();
    	
    	StatePlaying state = (StatePlaying) this.currentState;
    	state.setCurrentPosition(x, y);
    	
    }
    
    /**
     * Turns of graphics to dry-run controller for testing purposes.
     * This is irreversible. 
     */
    public void turnOffGraphics() {
    	panel = null;
    }
    
    //// Extension in preparation for Project 3: robot and robot driver //////
    /**
     * The robot that interacts with the controller starting from P3
     */
    Robot robot;
    /**
     * The driver that interacts with the robot starting from P3
     */
    RobotDriver driver;
    
    /**
     * The energy consumption of the robot, updated by the robot
     */
    float robotEnergyConsumption = 0;
    
    /**
     * A thread responsible for driving the robot in the background
     */
    Thread driverThread;
    
    /**
     * Sets the robot and robot driver
     * @param robot
     * @param robotdriver
     */
    public void setRobotAndDriver(Robot robot, RobotDriver robotdriver) {
        this.robot = robot;
        driver = robotdriver;
        
    }
    /**
     * set the driver
     * @param driver
     */
    public void setDriver(RobotDriver driver) {
    	this.driver = driver;
    }
    /**
     * @return the robot, may be null
     */
    public Robot getRobot() {
        return robot;
    }
    /**
     * @return the driver, may be null
     */
    public RobotDriver getDriver() {
        return driver;
    }
    
    /**
     * called by the robot to add energy consumption
     */
    protected void addEnergyConsumption(int energy) {
    	robotEnergyConsumption += energy;
    }
    
    /**
     * return energy consumed by robot
     * @return
     */
    public float getEnergyConsumption() {
    	return robotEnergyConsumption;
    }
    
    /**
     * Provides access to the maze configuration. 
     * This is needed for a robot to be able to recognize walls
     * for the distance to walls calculation, to see if it 
     * is in a room or at the exit. 
     * Note that the current position is stored by the 
     * controller. The maze itself is not changed during
     * the game.
     * This method should only be called in the playing state.
     * @return the MazeConfiguration
     */
    public Maze getMazeConfiguration() {
        return ((StatePlaying)states[2]).getMazeConfiguration();
    }
    /**
     * Provides access to the current position.
     * The controller keeps track of the current position
     * while the maze holds information about walls.
     * This method should only be called in the playing state.
     * @return the current position as [x,y] coordinates, 
     * 0 <= x < width, 0 <= y < height
     */
    public int[] getCurrentPosition() {
        return ((StatePlaying)states[2]).getCurrentPosition();
    }
    /**
     * Provides access to the current direction.
     * The controller keeps track of the current position
     * and direction while the maze holds information about walls.
     * This method should only be called in the playing state.
     * @return the current direction
     */
    public CardinalDirection getCurrentDirection() {
        return ((StatePlaying)states[2]).getCurrentDirection();
    }
    
    public void waitTillDriverFinished() {
    	if (null != driverThread) {
			try {
				driverThread.join();
			} catch (Exception e) { 
				System.out.println("join synchronization with driver thread lead to an exception") ;
			}
		}
		else {
			System.out.println("no thread to wait for");
		}
    }
    
    ////////////////////////////////////ActionListener related functionality/////////////////////////////////////////
    Thread leftThread;
	Thread rightThread;
	Thread forwardThread;
	Thread backwardThread;
    
	@Override
	public void actionPerformed(ActionEvent e) {
		String action = ((JButton)e.getSource()).getText();
		
		if (action == "Left") {
			if (leftThread == null) {
				System.out.println("Left Sensor Failer Thread Start");
				leftThread = new Thread(new RobotFailer(this, Direction.LEFT));
				leftThread.start();
			} else {
				System.out.println("Left Sensor Failer Thread Ended");
				leftThread.interrupt();
				leftThread = null;
			}
		}
		else if (action == "Right") {
			if (rightThread == null) {
				System.out.println("Right Sensor Failer Thread Start");
				rightThread = new Thread(new RobotFailer(this, Direction.RIGHT));
				rightThread.start();
			} else {
				System.out.println("Left Sensor Failer Thread Ended");
				rightThread.interrupt();
				rightThread = null;
			}
		}
		else if (action == "Forward") {
			if (forwardThread == null) {
				System.out.println("Forward Sensor Failer Thread Start");
				forwardThread = new Thread(new RobotFailer(this, Direction.FORWARD));
				forwardThread.start();
			} else {
				System.out.println("Left Sensor Failer Thread Ended");
				forwardThread.interrupt();
				forwardThread = null;
			}
		}
		else if (action == "Backward") {
			if (backwardThread == null) {
				System.out.println("Backward Sensor Failer Thread Start");
				backwardThread = new Thread(new RobotFailer(this, Direction.BACKWARD));
				backwardThread.start();
			} else {
				System.out.println("Left Sensor Failer Thread Ended");
				backwardThread.interrupt();
				backwardThread = null;
			}
		}
		
	}
	
	/**
	 * Clean up and stop all thread that is running
	 */
	public void stopAllThread() {
		if (leftThread != null) {
			leftThread.interrupt();
			leftThread = null;
		}
		if (rightThread != null) {
			rightThread.interrupt();
			rightThread = null;
		}
		if (forwardThread != null) {
			forwardThread.interrupt();
			forwardThread = null;
		}
		if (backwardThread != null) {
			backwardThread.interrupt();
			backwardThread = null;
		}
	}
	

}
