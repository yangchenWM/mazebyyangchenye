package gui;

import gui.Robot.Direction;

/**
 * 
 * this class is responsible for driving the robot to escape the maze by a wall follower algorithm
 * Depending on which sensors are available, the robotdriver will select different strategies of driving
 * the robot, which is encapsulated in the concept of modes. This class delegates the control to different
 * modes, which in turn drives the robot in the specific mode. When one mode is unfeasible due to sensor failure,
 * or a better mode is available, the robotdriver can be aware of the change and adapt mode accordingly
 * 
 * @author yangchen
 *
 */
public class WallFollower extends DefaultRobotDriver {
	
	/**
	 * the work is delegated to a list of other mode objects that have mode-specific behaviors
	 */
	private WallFollowerMode[] modes;
	/**
	 * The mode object that is working now
	 */
	private WallFollowerMode currentMode;
	
	public WallFollower() {
		super();
	}
	
	/**
	 * initialize the state pattern
	 */
	private void initializeMode() {
		modes = new WallFollowerMode[5];
		modes[0] = new WallFollowerModeLeftAndForward();
		modes[1] = new WallFollowerModeOnlyLeft();
		modes[2] = new WallFollowerModeForward();
		modes[3] = new WallFollowerModeOnlyRight();
		modes[4] = new WallFollowerModeOnlyBack();
		
		for (WallFollowerMode mode : modes) {
			mode.setDriver(this);
			mode.setRobot(robot);
		}
		
		currentMode = modes[0];
		
	}
	
	@Override
	/**
	 * swicth mode based on current information of what sensor is available
	 *
	 * principle: always choose the best mode available now
	 */
	public void triggerUpdateSensorInformation() {
		//if the left and forward sensor are both available, use the best mode
		if (robot.hasOperationalSensor(Direction.LEFT) && robot.hasOperationalSensor(Direction.FORWARD)) {
			setMode(modes[0]);
			return;
		}
		//if only left sensor is available, goo enough
		if (robot.hasOperationalSensor(Direction.LEFT)) {
			setMode(modes[1]);
			return;
		}
		//if only forward sensor is available, we can deal with it by 
		//a third mode
		if (robot.hasOperationalSensor(Direction.FORWARD)) {
			setMode(modes[2]);
			return;
		}
		//if none of the above mode is possible, try back mode
		if (robot.hasOperationalSensor(Direction.BACKWARD)) {
			setMode(modes[4]);
			return;
		}
		
		//the worst case, we have no choice but to rely on right sensor only and change the whole strategy
		setMode(modes[3]);
	}
	
	/**
	 * set the current mode of driving
	 * @param mode
	 */
	private void setMode(WallFollowerMode mode) {
		currentMode = mode;
	}
	
	@Override
	public boolean drive2Exit() throws Exception{
		
		//initialize the mode setting
		initializeMode();
		
		while (true) {
			
			//before every step, choose the best mode available now
			this.triggerUpdateSensorInformation();
			
			try {
				//delegate control to the mode
				currentMode.progressWithStrategy();
			} catch (AssertionError e) {
				//check if the game is won or lost
				if (robot.hasStopped()) return false;
				else return true;
			}
		}
		
	}
	
	
}
