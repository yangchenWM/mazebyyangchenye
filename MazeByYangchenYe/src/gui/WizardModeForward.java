package gui;

import gui.Robot.Direction;
import gui.Robot.Turn;

/**
 * 
 * This class provides functionality for the driver to know its distance in all four directions relying on a forward sensor
 * 
 * @author yangchen
 *
 */
public class WizardModeForward implements WizardMode {

	private Robot robot;
	private RobotDriver wizard;
	

	@Override
	public void setRobotAndDriver(Robot robot, RobotDriver driver) {
		
		this.robot = robot;
		this.wizard = driver;
		
	}
	
	@Override
	public boolean updateDistance(int[] distance) {
		
		try {
			
			distance[0] = robot.distanceToObstacle(Direction.FORWARD); //forward distance
			
		} catch(UnsupportedOperationException e) {
			wizard.triggerUpdateSensorInformation();
			return false;
		}
			
		robot.rotate(Turn.LEFT);
			
		try {
				
			distance[1] = robot.distanceToObstacle(Direction.FORWARD); //left distance
				
		} catch(UnsupportedOperationException e) {
			robot.rotate(Turn.RIGHT);
			wizard.triggerUpdateSensorInformation();
			return false;
		}
			
		robot.rotate(Turn.LEFT);
		
		try{
			
			distance[3] = robot.distanceToObstacle(Direction.FORWARD); //backward distance
			
		} catch (UnsupportedOperationException e) {
			robot.rotate(Turn.AROUND);
			wizard.triggerUpdateSensorInformation();
			return false;
		}
		
		robot.rotate(Turn.LEFT);
		
		try {
			distance[2] = robot.distanceToObstacle(Direction.FORWARD); //right distance
		} catch (UnsupportedOperationException e) {
			robot.rotate(Turn.LEFT);
			wizard.triggerUpdateSensorInformation();
			return false;
		}
		
		robot.rotate(Turn.LEFT);
			
		
		return true;
			

	}

}
