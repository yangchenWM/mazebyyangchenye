package gui;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import gui.Constants.UserMouseInput;

/**
 * 
 * Class implements a translation for the user input handled by the Controller class. 
 * The MazeApplication attaches the listener to the GUI, such that user mouse input
 * flows from GUI to the listener.actionPerformed to the Controller.actionPerformed method.
 * 
 * @author yangchen
 *
 */
public class SimpleActionListener implements ActionListener {
	
	Container parent;
	
	Controller controller;
	
	public SimpleActionListener(Container parent, Controller controller) {
		this.parent = parent;
		this.controller = controller;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		UserMouseInput uikey = null;
		int value = 0;
		
		JComboBox<String> choice;
		
		try{ //try to intepret event has choosing in a combo box
			choice = (JComboBox<String>)e.getSource();
		} catch(ClassCastException e1) { //if failed, then the event is clicking the start button
			uikey = UserMouseInput.Start;
			controller.actionPerformed(uikey, 0);
			return;
		}
		
		if (choice.getSelectedItem().getClass() == String.class) {
			String option = (String)choice.getSelectedItem();
			
			//select driver and builder
			switch(option) {
			case "DFS":
				uikey = UserMouseInput.DFS;
				break;
			case "Prim":
				uikey = UserMouseInput.Prim;
				break;
			case "Eller":
				uikey = UserMouseInput.Eller;
				break;
			case "Kruskal":
				uikey = UserMouseInput.Kruskal;
				break;
			case "HAK":
				uikey = UserMouseInput.HAK;
				break;
			case "WallFollower":
				uikey = UserMouseInput.WallFollower;
				break;
			case "Wizard":
				uikey = UserMouseInput.Wizard;
				break;
			case "Explorer":
				uikey = UserMouseInput.Explorer;
				break;
			case "Manual":
				uikey = UserMouseInput.Manual;
				break;
			default:
				break;
			}
		} else {
			//select skill level
			int code = (int)choice.getSelectedItem();
		
			if (code >= 1 && code <= 15) {
				uikey = UserMouseInput.level;
				value = code;
			}
		
		}

		
		if (uikey != null) {
			controller.actionPerformed(uikey, value);
		}
		else {
			System.out.println("Invalid input");
		}

	}

}
