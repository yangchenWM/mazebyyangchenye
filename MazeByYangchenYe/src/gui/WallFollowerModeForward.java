package gui;

import gui.Robot;

import gui.RobotDriver;
import gui.WallFollowerMode;
import gui.Robot.Direction;
import gui.Robot.Turn;

/**
 * 
 * This class implements the WallFollowerMode interface in a mode where the robot's forward sensor
 * is available but left sensor is not 
 * In this case, the guiding strategy is still to turn left whenever possible. Since the left
 * sensor is not available, whenever we neeed to know the left distance, the robot should first rotate
 * left, and then use its forward sensor to do the job, and then rotate back right. The way of finding
 * out forward distance is unchanged
 * 
 * @author yangchen
 *
 */
public class WallFollowerModeForward implements WallFollowerMode {

	private Robot robot;
	private RobotDriver driver;
	
	
	@Override
	public void setRobot(Robot robot) {
		this.robot = robot;

	}

	@Override
	public void setDriver(RobotDriver driver) {
		this.driver = driver;
	}

	@Override
	public int findOutForwardDistance() throws AssertionError, UnsupportedOperationException {
		return robot.distanceToObstacle(Direction.FORWARD);
	}

	@Override
	public int findOutLeftDistance() throws AssertionError, UnsupportedOperationException {
		robot.rotate(Turn.LEFT);
		int result = robot.distanceToObstacle(Direction.FORWARD);
		robot.rotate(Turn.RIGHT);
		return result;
	}

	@Override
	//not used in this mode
	public int findOutRightDistance() throws AssertionError, UnsupportedOperationException {
		throw new RuntimeException("not implemented");
	}

	@Override
	public void progressWithStrategy() throws AssertionError, UnsupportedOperationException {
		
		try {
			
			int leftDistance = findOutLeftDistance();
			int forwardDistance = findOutForwardDistance();
			//not possible to go left or go forward, turn right
			if (leftDistance == 0 && forwardDistance == 0) {
				robot.rotate(Turn.RIGHT);
			}
			else if (leftDistance == 0) {//not possible to go left, possible to go forward
				robot.move(1);
			}
			else {//possible to go left
				robot.rotate(Turn.LEFT);
				robot.move(1);
			}
			
		} catch(UnsupportedOperationException e) { //mode no longer feasible, trigger the driver to deal with it
			driver.triggerUpdateSensorInformation();
		}

	}


}
