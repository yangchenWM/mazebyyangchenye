package gui;

import gui.Robot.Direction;
import gui.Robot.Turn;

/**
 * 
 * This class implements the WallFollowerMode interface in a mode where the robot's forward sensor
 * is unavailable but its left sensor is. 
 * In this case, the guiding strategy is still to turn left whenever possible. Since the forward
 * sensor is not available, we choose not to detect forward distance at all. instead, we keep track
 * of the current position before and after move, if the currentposition does not change, we know that
 * there is a wall in the forward, and the robot will turn right.
 * This will cause the robot to go one step further against the wall and waste 5 more energy, but it is
 * still much cheaper than rotating 2 more times every step
 * 
 * @author yangchen
 *
 */
public class WallFollowerModeOnlyLeft implements WallFollowerMode {
	
	private Robot robot;
	private RobotDriver driver;
	
	@Override
	public void setRobot(Robot robot) {
		this.robot = robot;

	}

	@Override
	public void setDriver(RobotDriver driver) {
		this.driver = driver;
	}

	@Override
	//will not use this method in this mode
	public int findOutForwardDistance() throws AssertionError, UnsupportedOperationException {
		throw new RuntimeException("not implemented");
	}

	@Override
	public int findOutLeftDistance() throws AssertionError, UnsupportedOperationException {
		return robot.distanceToObstacle(Direction.LEFT);
	}

	@Override
	//will not use this method in this mode
	public int findOutRightDistance() throws AssertionError, UnsupportedOperationException {
		throw new RuntimeException("not implemented");
	}

	@Override
	public void progressWithStrategy() throws AssertionError, UnsupportedOperationException {
		
		try {
			int[] currentPosition = robot.getCurrentPosition();
			int leftDistance = findOutLeftDistance();
			if (leftDistance == 0) { //possible to go forward or turn right
				robot.move(1);
				int[] newPosition = robot.getCurrentPosition();
				//check to see if the move suceeded, if not, turn right
				if (newPosition[0] == currentPosition[0] && newPosition[1] == currentPosition[1])
					robot.rotate(Turn.RIGHT);
			}
			else { //possible to go left
				robot.rotate(Turn.LEFT);
				robot.move(1);
			}
		} catch(UnsupportedOperationException e) { //the sensor failed and mode unfeasible, handle control to the
													//driver to deal with
			driver.triggerUpdateSensorInformation();
		}

	}

}
