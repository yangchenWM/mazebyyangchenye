package gui;

import java.util.ArrayList;
import java.util.Collections;

import generation.CardinalDirection;
import gui.Robot.Direction;
import gui.Robot.Turn;

/**
 * This class instantiates the robot driver interface using the explorer algorithm that solves the maze
 * relying on a memory of how often it has been in a given cell before. It has three strategies
 * 1. If it is at the exit position or can see the exit position, it goes for it
 * 2. If it is outside a room, it scans possible neighbours and go for the one least visited
 * 3. If it is inside a room, it scans the room to find the least visited doors and go for that door
 * to leave the room
 * 
 * @author yangchen
 *
 */
public class Explorer extends DefaultRobotDriver {
	
	private ArrayList<Cell> listOfNeighbourByWalk;
	
	int[][] timeOfVisit;
	
	public Explorer() {
		super();
		listOfNeighbourByWalk = new ArrayList<Cell>();
	}
	
	@Override
	public boolean drive2Exit() {
		
		//initialize memory
		timeOfVisit = new int[width][height];
		
		while (true) {
			//if exit can be seen
			if (robot.canSeeThroughTheExitIntoEternity(Direction.FORWARD)) {
				try {
					robot.move(height+width);
				} catch (AssertionError e) {
					return !robot.hasStopped();
				}
			}
			if (robot.canSeeThroughTheExitIntoEternity(Direction.LEFT)) {
				try {
					robot.rotate(Turn.LEFT);
					robot.move(height+width);
				} catch (AssertionError e) {
					return !robot.hasStopped();
				}
			}
			if (robot.canSeeThroughTheExitIntoEternity(Direction.RIGHT)) {
				try {
					robot.rotate(Turn.RIGHT);
					robot.move(height+width);
				} catch (AssertionError e) {
					return !robot.hasStopped();
				}
			}
			if (robot.canSeeThroughTheExitIntoEternity(Direction.BACKWARD)) {
				try {
					robot.rotate(Turn.AROUND);
					robot.move(height+width);
				} catch (AssertionError e) {
					return !robot.hasStopped();
				}
			}
			
			//visit one more time
			int[] cur = robot.getCurrentPosition();
			Cell currentCell = new Cell(cur[0], cur[1]);
			timeOfVisit[cur[0]][cur[1]]++;
			
			//choose what to do
			try{
				updateNeighbour(currentCell);
			} catch(AssertionError e) {
				return false;
			}
			
			sortBytimeOfVisit(this.listOfNeighbourByWalk);
			
			Cell cellToGo = listOfNeighbourByWalk.get(0);
			
			try {
				if (cellToGo.d != Direction.FORWARD) timeOfVisit[cur[0]][cur[1]]++;
				walkToCell(cellToGo.d);
			} catch(AssertionError e) {
				return false;
			}
			
			listOfNeighbourByWalk.clear();
			
		}
		
		
	}
	
	/**
	 * private helper method to walk one step through the given direction
	 * @param d
	 */
	private void walkToCell(Direction d) throws AssertionError{
		
		switch (d) {
		case FORWARD:
			robot.move(1);
			break;
		case LEFT:
			robot.rotate(Turn.LEFT);
			robot.move(1);
			break;
		case RIGHT:
			robot.rotate(Turn.RIGHT);
			robot.move(1);
			break;
		case BACKWARD:
			robot.rotate(Turn.AROUND);
			robot.move(1);
			break;
		}
		
	}
	
	private void sortBytimeOfVisit(ArrayList<Cell> list) {
		//randomize
		Collections.shuffle(list);
		
		//perform an insertion sort
		for (int i = 1; i < list.size(); ++i) {
			Cell pivot = list.get(i);
			int pivotTime = timeOfVisit[pivot.X][pivot.Y];
			int j = i;
			while (j > 0 && timeOfVisit[list.get(j-1).X][list.get(j-1).Y] > pivotTime) {
				list.set(j, list.get(j-1));
				--j;
			}
			list.set(j, pivot);
		}
		
	}
	
	/**
	 * provide helper functionality to update the list of neighbours of the given cell
	 * 1. if forward distance is not 0 (no wall in front), put the forward neighbour in
	 * the list of walk, otherwise in the list of jump
	 * 2. do the same for all four directions
	 * @param cell
	 */
	private void updateNeighbour(Cell cell) {
		
		//calculate forward neighbour
		CardinalDirection forwardDirection = robot.getCurrentDirection();
		int[] d = forwardDirection.getDirection();
		Cell forwardNeighbour = new Cell(cell.X + d[0], cell.Y + d[1], Direction.FORWARD);
		
		
		//calculate left neighbour
		CardinalDirection leftDirection = forwardDirection.rotateClockwise();
		d = leftDirection.getDirection();
		Cell leftNeighbour = new Cell(cell.X + d[0], cell.Y + d[1], Direction.LEFT);
		
		//calculate right neighbour
		CardinalDirection rightDirection = leftDirection.oppositeDirection();
		d = rightDirection.getDirection();
		Cell rightNeighbour = new Cell(cell.X + d[0], cell.Y + d[1], Direction.RIGHT);
		
		//calculate back neighbour
		CardinalDirection backDirection = forwardDirection.oppositeDirection();
		d = backDirection.getDirection();
		Cell backNeighbour = new Cell(cell.X + d[0], cell.Y + d[1], Direction.BACKWARD);
		
		
		//calculate if there is a wall in between
		//forward no wall, cell valid
		if (isValidCell(forwardNeighbour) && findOutForwardDistance() != 0) {
			this.listOfNeighbourByWalk.add(forwardNeighbour);
		} 
		
		//left no wall, cell valid
		if (isValidCell(leftNeighbour) && findOutLeftDistance() != 0) {
			this.listOfNeighbourByWalk.add(leftNeighbour);
		} 
		
		//right no wall, cell valid
		if (isValidCell(rightNeighbour) && findOutRightDistance() != 0) {
			this.listOfNeighbourByWalk.add(rightNeighbour);
		} 
		
		if (listOfNeighbourByWalk.size() != 0) return;
		
		//backward no wall, cell valid
		if (isValidCell(backNeighbour) && findOutBackwardDistance() != 0) {
			this.listOfNeighbourByWalk.add(backNeighbour);
		} 
		
	}
	
	/**
	 * private helper method to let the  driver figure out how far it is from a forward wall.
	 * It tries to use its forward sensor, but if that failed, it tries to use its other sensors
	 * in turn, assuming that at least one sensor will be operational at any time.
	 * @return
	 */
	private int findOutForwardDistance() {
		
		try {
			return robot.distanceToObstacle(Direction.FORWARD);
		} catch(UnsupportedOperationException e) {
			robot.rotate(Turn.LEFT);
		}
		
		try {
			int result = robot.distanceToObstacle(Direction.RIGHT);
			robot.rotate(Turn.RIGHT);
			return result;
		} catch(UnsupportedOperationException e) {
			robot.rotate(Turn.LEFT);
		}
		
		try {
			int result = robot.distanceToObstacle(Direction.BACKWARD);
			robot.rotate(Turn.AROUND);
			return result;
		} catch(UnsupportedOperationException e) {
			robot.rotate(Turn.LEFT);
		}
		
		try{
			int result = robot.distanceToObstacle(Direction.LEFT);
			robot.rotate(Turn.LEFT);
			return result;
		} catch(UnsupportedOperationException e) {
			return findOutForwardDistance();
		}
		
	}
	
	/**
	 * private helper method to let the  driver figure out how far it is from a left wall.
	 * It tries to use its forward sensor, but if that failed, it tries to use its other sensors
	 * in turn, assuming that at least one sensor will be operational at any time.
	 * @return
	 */
	private int findOutLeftDistance() {
		
		try {
			return robot.distanceToObstacle(Direction.LEFT);
		} catch(UnsupportedOperationException e) {
			robot.rotate(Turn.LEFT);
		}
		
		try {
			int result = robot.distanceToObstacle(Direction.FORWARD);
			robot.rotate(Turn.RIGHT);
			return result;
		} catch(UnsupportedOperationException e) {
			robot.rotate(Turn.LEFT);
		}
		
		try {
			int result = robot.distanceToObstacle(Direction.RIGHT);
			robot.rotate(Turn.AROUND);
			return result;
		} catch(UnsupportedOperationException e) {
			robot.rotate(Turn.LEFT);
		}
		
		try{
			int result = robot.distanceToObstacle(Direction.BACKWARD);
			robot.rotate(Turn.LEFT);
			return result;
		} catch(UnsupportedOperationException e) {
			return findOutLeftDistance();
		}
		
	}
	
	/**
	 * private helper method to let the  driver figure out how far it is from a right wall.
	 * It tries to use its forward sensor, but if that failed, it tries to use its other sensors
	 * in turn, assuming that at least one sensor will be operational at any time.
	 * @return
	 */
	private int findOutRightDistance() {
		
		try {
			return robot.distanceToObstacle(Direction.RIGHT);
		} catch(UnsupportedOperationException e) {
			robot.rotate(Turn.LEFT);
		}
		
		try {
			int result = robot.distanceToObstacle(Direction.BACKWARD);
			robot.rotate(Turn.RIGHT);
			return result;
		} catch(UnsupportedOperationException e) {
			robot.rotate(Turn.LEFT);
		}
		
		try {
			int result = robot.distanceToObstacle(Direction.LEFT);
			robot.rotate(Turn.AROUND);
			return result;
		} catch(UnsupportedOperationException e) {
			robot.rotate(Turn.LEFT);
		}
		
		try{
			int result = robot.distanceToObstacle(Direction.FORWARD);
			robot.rotate(Turn.LEFT);
			return result;
		} catch(UnsupportedOperationException e) {
			return findOutRightDistance();
		}
		
	}
	
	/**
	 * private helper method to let the  driver figure out how far it is from a backward wall.
	 * It tries to use its forward sensor, but if that failed, it tries to use its other sensors
	 * in turn, assuming that at least one sensor will be operational at any time.
	 * @return
	 */
	private int findOutBackwardDistance() {
		
		try {
			return robot.distanceToObstacle(Direction.BACKWARD);
		} catch(UnsupportedOperationException e) {
			robot.rotate(Turn.LEFT);
		}
		
		try {
			int result = robot.distanceToObstacle(Direction.LEFT);
			robot.rotate(Turn.RIGHT);
			return result;
		} catch(UnsupportedOperationException e) {
			robot.rotate(Turn.LEFT);
		}
		
		try {
			int result = robot.distanceToObstacle(Direction.FORWARD);
			robot.rotate(Turn.AROUND);
			return result;
		} catch(UnsupportedOperationException e) {
			robot.rotate(Turn.LEFT);
		}
		
		try{
			int result = robot.distanceToObstacle(Direction.RIGHT);
			robot.rotate(Turn.LEFT);
			return result;
		} catch(UnsupportedOperationException e) {
			return findOutBackwardDistance();
		}
		
	}
	
	
	/**
	 * this method determines if a cell is a valid position in the maze
	 * @param cell
	 * @return
	 */
	private boolean isValidCell(Cell cell) {
		if (cell.X < 0 || cell.X >= width) return false;
		if (cell.Y < 0 || cell.Y >= height) return false;
		return true;
	}
	
	/**
	 * 
	 * simple abstraction of the positions in the maze
	 * 
	 * @author yangchen
	 *
	 */
	private class Cell{
		private int X;
		private int Y;
		private Direction d;
		
		//create the starting cell
		public Cell(int x, int y) {
			X = x;
			Y = y;
			d = null;
		}
		
		//create other cells that bears a relation to the cell visited before it
		public Cell(int x, int y, Direction d) {
			X = x;
			Y = y;
			this.d = d;
		}
		
	}
}
