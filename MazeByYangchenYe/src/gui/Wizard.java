package gui;

import java.util.ArrayList;

import generation.CardinalDirection;
import gui.Robot.Direction;
import gui.Robot.Turn;

/**
 * 
 * this class has the responsibility to drive a robot to escape the maze by a wizard algorithm
 * a wizard algorithm solves the maze relying on a distance matrix given
 * 1. in any given position, scan through the distance of its neighbouring positions and find the 
 * optimal one to go
 * 1.1 an optimal one is not necessarily the one with smallest distance, because that position might
 * need a jump to go to, which is much more expensive than a move. Therefore, a jump is endorsed only
 * if the benefit of it outweighs the expensiveness
 * 2. repeat 1 until we're at a position with distance 1, i.e., the exit position
 * 3. at the exit position, scan to find the exit, and go through it, done
 * 
 * @author yangchen
 *
 */
public class Wizard extends DefaultRobotDriver {
	
	/**
	 * maintain a list of neighbours that one can walk to
	 */
	private ArrayList<Cell> listOfNeighbourByWalk;
	
	/**
	 * maintain a list of neighbours that one can only jump to,
	 * that is, there is a wall in between
	 */
	private ArrayList<Cell> listOfNeighbourByJump;
	
	/**
	 * Keep track of the distance to the four direction each time the
	 * driver makes a decision where to go
	 * distanceList[0]: forward
	 * distanceList[1]: left
	 * distanceList[2]: right
	 * distanceList[3]: backward
	 */
	private int[] distanceList;
	
	/**
	 * wizard class uses an array of mode object to handle the distance calculating work
	 * modes[0]: all sensors
	 * modes[1]: only forward sensor
	 * modes[2]: only left sensor
	 * modes[3]: only right sensor
	 * modes[4]: only backward sensor
	 * modes[5]: forward and backward sensor
	 * modes[6]: left and right sensor
	 */
	private WizardMode[] modes;
	
	/**
	 * the current mode that is working
	 */
	private WizardMode currentMode;
	
	
	public Wizard() {
		super();
		
		listOfNeighbourByWalk = new ArrayList<Cell>();
		listOfNeighbourByJump = new ArrayList<Cell>();
		
		distanceList = new int[4];
	}
	
	@Override
	public void triggerUpdateSensorInformation() {
		
		if (robot.hasOperationalSensor(Direction.FORWARD) && robot.hasOperationalSensor(Direction.LEFT)
				&& robot.hasOperationalSensor(Direction.RIGHT) && robot.hasOperationalSensor(Direction.BACKWARD)) {
			currentMode = modes[0]; //use the best mode of all sensors
			return;
		}
		
		if (robot.hasOperationalSensor(Direction.FORWARD) && robot.hasOperationalSensor(Direction.BACKWARD)) {
			currentMode = modes[5]; //use the mode of forward and backward sensor
			return;
		}
		
		if (robot.hasOperationalSensor(Direction.LEFT) && robot.hasOperationalSensor(Direction.RIGHT)) {
			currentMode = modes[6]; //use the mode of left and right sensor
			return;
		}
		
		if (robot.hasOperationalSensor(Direction.FORWARD)) {
			currentMode = modes[1]; //use the forward mode
			return;
		}
		
		if (robot.hasOperationalSensor(Direction.LEFT)) {
			currentMode = modes[2]; //use the left mode
			return;
		}
		
		if (robot.hasOperationalSensor(Direction.RIGHT)) {
			currentMode = modes[3]; //use the right mode
			return;
		}
		
		currentMode = modes[4]; //use the backward mode
	}
	
	
	@Override
	public boolean drive2Exit() {
		
		//precondition: the robot should have been started
		if (!started) return false;
		
		//initialize mode objects and setups 
		initializeMode();
		
		try {
			
			//proceed using wizard algorithm while the robot is not at exit
			while (true) {
				
				triggerUpdateSensorInformation();
				
				//delegate the work of updating distance to the mode object
				//only proceed if the updating is successful
				if (!currentMode.updateDistance(distanceList)) continue;
				
				//make one step further based on the information provided
				//by the mode object
				this.progressOneStep();
			
				//clear list
				listOfNeighbourByWalk.clear();
				listOfNeighbourByJump.clear();
			

				if (robot.isAtExit()) break;

			
			}
			
		} catch(AssertionError e) { //the robot is not at exit, the assertionerror can only due to lack of energy
			return false;
		}
		
		//at the exit position, go for the exit
		try{
			
			goThroughExit();
			
		} catch(AssertionError e) { //robot has stopped, game lost
			return false;
		}
		
		//all methods successfully returned, game won
		return true;
		
	}
	
	private void initializeMode() {
		modes = new WizardMode[7];
		
		//instantiate mode objects
		modes[0] = new WizardModeAllSensor();
		modes[1] = new WizardModeForward();
		modes[2] = new WizardModeLeft();
		modes[3] = new WizardModeRight();
		modes[4] = new WizardModeBackward();
		modes[5] = new WizardModeForwardAndBackward();
		modes[6] = new WizardModeLeftAndRight();
		
		//connect mode object to the robot and this driver
		for (WizardMode mode: modes) {
			mode.setRobotAndDriver(robot, this);
		}
	}
	
	/**
	 * This function encapsulates one step made by the wizard, which consists
	 * in scanning its neighbour, decide which one to go, and go for it
	 */
	private void progressOneStep() {
		
		//initialize starting position
		Cell currentPosition = new Cell(robot.getCurrentPosition()[0], robot.getCurrentPosition()[1]);

		//update list of neighbours
		updateNeighbour(currentPosition);
	
		//sort the list by distance to choose where to go
		sortByDistance(this.listOfNeighbourByJump);
		sortByDistance(this.listOfNeighbourByWalk);
	
		//two candidates: the cells with least distance
		//in two lists
		//note: possible to have no candidaeJump because robot can be in a room
		Cell candidateWalk = listOfNeighbourByWalk.get(0);
		Cell candidateJump = listOfNeighbourByJump.size() > 0 ? listOfNeighbourByJump.get(0) : null;
	
		if (candidateJump == null) { //the robot is in a room, no jump is possible
		
			walkToCell(candidateWalk.d);
		}
		else { // otherwise, choose the optimal position to go to
			//select which one to go for, criterion:
			//if by jumping the distance is 8 or more 
			//less than the distance of walking, jump,
			//otherwise walk
			int distanceJump = distance.getDistanceValue(candidateJump.X, candidateJump.Y);
			int distanceWalk = distance.getDistanceValue(candidateWalk.X, candidateWalk.Y);
		
			if (distanceJump <= distanceWalk - 8) jumpToCell(candidateJump.d);
			else walkToCell(candidateWalk.d);
		}
		
	}
	
	/**
	 * The final step of going out of the maze
	 * precondition: the robot should be at the exit position
	 */
	private void goThroughExit() throws AssertionError{
	
		assert robot.isAtExit();
		
		if (robot.canSeeThroughTheExitIntoEternity(Direction.FORWARD)) robot.move(1);
		else if (robot.canSeeThroughTheExitIntoEternity(Direction.LEFT)) {
			robot.rotate(Turn.LEFT);
			robot.move(1);;
		}
		else if (robot.canSeeThroughTheExitIntoEternity(Direction.RIGHT)) {
			robot.rotate(Turn.RIGHT);
			robot.move(1);
		}
		else {
			robot.rotate(Turn.AROUND);
			robot.move(1);
		}
	}
	
	/**
	 * private helper method to walk one step through the given direction
	 * @param d
	 */
	private void walkToCell(Direction d) throws AssertionError{
		
		switch (d) {
		case FORWARD:
			robot.move(1);
			break;
		case LEFT:
			robot.rotate(Turn.LEFT);
			robot.move(1);
			break;
		case RIGHT:
			robot.rotate(Turn.RIGHT);
			robot.move(1);
			break;
		case BACKWARD:
			robot.rotate(Turn.AROUND);
			robot.move(1);
			break;
		}
		
	}
	
	/**
	 * private helper method to jump to the given direction
	 * @param d
	 * @throws AssertionError if the robot went out of energy on
	 */
	private void jumpToCell(Direction d) throws AssertionError{
		
		switch (d) {
		case FORWARD:
			robot.jump();
			break;
		case LEFT:
			robot.rotate(Turn.LEFT);
			robot.jump();
			break;
		case RIGHT:
			robot.rotate(Turn.RIGHT);
			robot.jump();
			break;
		case BACKWARD:
			robot.rotate(Turn.AROUND);
			robot.jump();
			break;
		}
		
	}
	

	/**
	 * provide helper functionality to update the list of neighbours of the given cell
	 * 1. if forward distance is not 0 (no wall in front), put the forward neighbour in
	 * the list of walk, otherwise in the list of jump
	 * 2. do the same for all four directions
	 * @param cell
	 */
	private void updateNeighbour(Cell cell) {
		
		//calculate forward neighbour
		CardinalDirection forwardDirection = robot.getCurrentDirection();
		int[] d = forwardDirection.getDirection();
		Cell forwardNeighbour = new Cell(cell.X + d[0], cell.Y + d[1], Direction.FORWARD);
		
		
		//calculate left neighbour
		CardinalDirection leftDirection = forwardDirection.rotateClockwise();
		d = leftDirection.getDirection();
		Cell leftNeighbour = new Cell(cell.X + d[0], cell.Y + d[1], Direction.LEFT);
		
		//calculate right neighbour
		CardinalDirection rightDirection = leftDirection.oppositeDirection();
		d = rightDirection.getDirection();
		Cell rightNeighbour = new Cell(cell.X + d[0], cell.Y + d[1], Direction.RIGHT);
		
		//calculate back neighbour
		CardinalDirection backDirection = forwardDirection.oppositeDirection();
		d = backDirection.getDirection();
		Cell backNeighbour = new Cell(cell.X + d[0], cell.Y + d[1], Direction.BACKWARD);
		
		
		//calculate if there is a wall in between
		//forward no wall, cell valid
		if (isValidCell(forwardNeighbour) && distanceList[0] != 0) {
			this.listOfNeighbourByWalk.add(forwardNeighbour);
		} else if (isValidCell(forwardNeighbour)) {//forward has a wall, cell valid
			this.listOfNeighbourByJump.add(forwardNeighbour);
		}
		
		//left no wall, cell valid
		if (isValidCell(leftNeighbour) && distanceList[1] != 0) {
			this.listOfNeighbourByWalk.add(leftNeighbour);
		} else if (isValidCell(leftNeighbour)){//left has a wall, cell valid
			this.listOfNeighbourByJump.add(leftNeighbour);
		}
		
		//right no wall, cell valid
		if (isValidCell(rightNeighbour) && distanceList[2] != 0) {
			this.listOfNeighbourByWalk.add(rightNeighbour);
		} else if (isValidCell(rightNeighbour)){//right has a wall, cell valid
			this.listOfNeighbourByJump.add(rightNeighbour);
		}
		
		//backward no wall, cell valid
		if (isValidCell(backNeighbour) && distanceList[3] != 0) {
			this.listOfNeighbourByWalk.add(backNeighbour);
		} else if (isValidCell(backNeighbour)){//forward has a wall, cell valid
			this.listOfNeighbourByJump.add(backNeighbour);
		}
		
	}
	
	/**
	 * provide helper functionality to sort a list of cells by their distance to
	 * the exit position
	 * Insertion sort suffices the need because the list is bound to be
	 * very small (at most 4 elements)
	 * @param list
	 */
	private void sortByDistance(ArrayList<Cell> list) {
		
		for (int i = 1; i < list.size(); ++i) {
			Cell pivotCell = list.get(i);
			int j = i;
			while (j > 0 
				&& distance.getDistanceValue(list.get(j-1).X, list.get(j-1).Y) > distance.getDistanceValue(pivotCell.X,pivotCell.Y)) {
				list.set(j, list.get(j-1));
				--j;
			}
			list.set(j, pivotCell);
		}
		
		
	}
	
	/**
	 * this method determines if a cell is a valid position in the maze
	 * @param cell
	 * @return
	 */
	private boolean isValidCell(Cell cell) {
		if (cell.X < 0 || cell.X >= width) return false;
		if (cell.Y < 0 || cell.Y >= height) return false;
		return true;
	}
	
	
	/**
	 * 
	 * simple abstraction of the positions in the maze
	 * 
	 * @author yangchen
	 *
	 */
	private class Cell{
		private int X;
		private int Y;
		private Direction d;
		
		//create the starting cell
		public Cell(int x, int y) {
			X = x;
			Y = y;
			d = null;
		}
		
		//create other cells that bears a relation to the cell visited before it
		public Cell(int x, int y, Direction d) {
			X = x;
			Y = y;
			this.d = d;
		}
		
	}
}
